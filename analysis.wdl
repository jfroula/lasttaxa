workflow jgi_lasttaxa {
    String fastalist
    String accessions
    String taxonomy
    String nr
    Int  threads
	Array[Array[File]] list = read_tsv(fastalist)

   scatter (fasta in list) {
   	   call run_lastTaxa {
          input: fasta=fasta[0],
             accessions=accessions,
             taxonomy=taxonomy,
             nr=nr,
             threads=threads
   	   }
   }
}

task run_lastTaxa {
    File fasta
    String accessions
    String taxonomy
    String nr
    Int  threads
	String bname = basename(fasta)
     
    command
    {    
        shifter --image=jfroula/lasttaxa:latest lastTaxa.sh \
			-o ${bname}_dir \
			-q ${accessions} \
			-d ${nr} \
			-t ${taxonomy} \
			-c ${threads} \
			${fasta}
	}
    output {
       File results_array = "${bname}_dir"
    }
}


