# Summary #

## Purpose ###
To predict taxonomy classification for genomes or genome bins derived from metagenome binning. 
It has two major steps:  

1) predict proteins using prodigal (http://prodigal.ornl.gov/) & annotate them by aligning the proteins to NCBI's NR database using lastal (http://last.cbrc.jp/).     
2) each metagenome bin is assigned to the taxonomy that the majority of its proteins mapped to.

# Installation

### Prerequisites to run lastTaxa.sh  
I use a docker image to run lasttaxa so you don't need to worry about installing dependencies.  
You can check the Dockerfile to see what was installed to create the image.

If you don't have docker installed, you can use the Dockerfile to find pre-requisites and install them using conda (i.e. miniconda or anaconda), or any other way you prefer.

You can run these commands using "docker run" except on NERSC machines which uses shifter instead.  My examples use shifter but docker can be used instead (I give one docker example below that you can adapt).  

### Download the docker image  
On machines where docker is installed
```
docker pull jfroula/lasttaxa:1.1.8
```

On NERSC machines
```
shifterimg pull jfroula/lasttaxa:1.1.8
```

# Usage

### If you want to process one bin at a time
run `bin/lastTaxa.sh` without arguments to see help.  

```
# example
shifter --image=jfroula/lasttaxa:latest lastTaxa.sh \
-o testout_dir \
-q accessions.db \
-d nr-last/nr \
-t NCBI_taxonomy \
-c 8 \
yourdata.fasta
```

NOTE: if you are using docker then the above command would be like the following example. Notice that you have to mount the current working directory to some directory in the container (e.g. /home)  
```
docker run --volume="$(pwd):/home" jfroula/lasttaxa:latest lastTaxa.sh \
-o /home/testout_dir \
-q /home/accessions.db \
-d /home/nr-last/nr \
-t /home/NCBI_taxonomy \
-c 8 \
/home/yourdata.fasta
```

### If you want to process multiple bins at one time
run `bin/lastTaxa_jobArray_denovo.sh` without arguments to see help.  

#### use sbatch system (set up for DENOVO).  
There is one caveat. `You need to have the NCBI_taxonomy directory someplace writable (i.e not on dtn.nersc.gov)`
```
sbatch --array=1-10 bin/lastTaxa_jobArray_denovo.sh \
-d nr-last/nr \
-t NCBI_taxonomy \
-q accessions.db \
-c 4 \
list_of_fasta_paths
```

#### You can also use cromwell to process multiple bins.  
```
java -jar <path_to_cromwell>/cromwell.jar run --inputs inputs.json analysis.wdl
```  
Here is an example of  "inputs.json"
(see below for explainations of each input)
```
{
    "jgi_lasttaxa.nr": "<full_path>/<directory_that_lastdb_created>/nr",
    "jgi_lasttaxa.taxonomy": "<some_local_writable_path>/NCBI_taxonomy",
    "jgi_lasttaxa.accessions": "<full_path>/accessions.db",
    "jgi_lasttaxa.threads": 64,
    "jgi_lasttaxa.fastalist": "<full_path>/list"
}
```  

1. the nr database is formatted by lastdb (see below).  Note that I have "<full_path>/db_directory/nr". 
   The "nr" is the prefix for all the files created by lastdb and "nr" itself doesn't exist.
2. the NCBI_taxonomy db must be in a directory that you can write to.  
3. the file "list" is a file of one or more paths to a fasta file that you want to classify.



# Building Required Databases

### Summary of required databases:

You need to create:  
1) formatted nr database created by lastdb   
2) the taxonomy dump tables nodes.dmp and names.dmp as well as the sql versions nodes.db and names.db.   
3) sqlite3 table of NCBI's "accessions-to-taxonomy" ("accessions.db")  

#### Before you create any reference databases
you need to have the following dependencies installed

```
# Clone the lasttaxa repo:
git clone https://gitlab.com/jfroula/lasttaxa

# Install necessary perl modules:
sudo -H cpan install DBI
sudo -H cpan install DBD::SQLite

... or using conda

conda install -y -c bioconda perl-dbi==1.642-0
conda install -y -c bioconda perl-dbd-sqlite

# Install LAST:
wget http://last.cbrc.jp/last-982.zip
unzip last-982.zip
cd last-982 && make
# The compiled program is in last-982/src/
```

Now you can install NR and taxanomy databases as directed below.


#### NR database  

```
# First download the nr fasta file from NCBI
wget ftp://ftp.ncbi.nih.gov/blast/db/FASTA/nr.gz
gunzip nr.gz

# Then create a last database  
# assuming last/bin is in your PATH
# When runing lastdb to format the NR database, make sure to use a node with more than 20GB.
# This step takes a very long time and a very large space. 

last-982/src/lastdb -P8 lastdb -cR01 -p -c -m110 nr nr
```

You can create your own protein database.  The only requirement is that the fasta headers must have the protein accession number as the first non space element after the ">". (i.e. ">WC_23432.2 ") which is the new default for genbank and refseq (as of 3/27/2017).  

#### Taxonomic files
The taxonomic tables are downloaded from NCBI.  
```
mkdir ncbi_taxdump && cd ncbi_taxdump
wget ftp://ftp.ncbi.nih.gov/pub/taxonomy/taxdump.tar.gz
tar zxvf taxdump.tar.gz 
rm citations.dmp delnodes.dmp division.dmp gc.prt gencode.dmp merged.dmp readme.txt taxdump.tar.gz
```

Now you need to make an sqlite3 database for the two tables, nodes.dmp and names.dmp.  
```
cd ncbi_taxdump
<repository>/bin/uploadnodes.pl > /dev/null &
<repository>/bin/uploadnames.pl > /dev/null &
```

#### accession.db (sqlite3 database)
This database is used to find taxonomic IDs from protein accession numbers found in the fasta headers.  

```
# download table and re-format it to save memory.
# you can create this file anywhere
wget ftp://ftp.ncbi.nih.gov/pub/taxonomy/accession2taxid/prot.accession2taxid.gz
zcat prot.accession2taxid.gz | awk -F"\t" '{print $2"|"$3}' > acc2taxid

# this command creates the accessions.db but takes awhile. 
sqlite3 accessions.db < <repository>/bin/sql_accessions.sh
rm prot.accession2taxid.gz acc2taxid
```
----------------------------
# Interpreting Results
The pertinent files are  
1. finalSummary  
2. classificationByProtein  
3. classificationByRank   
4. Plots/*.png   

#### Description of finalSummary
Summary of the best guess (i.e. top line of classificationByProtein) as well as some stats to help you get an idea how confident you can be with the prediction.  For example, this table tells you the percentage of proteins assigned to the most likely taxa of each rank.  

#### Description of classificationByProtein
This table is a list of species, from most protein hits to least.  This table was used in the creation of "finalSummary" and may not be of much use to you on its own.

##### columns are:  
1) protein hits to taxa  
2) taxa full path

#### Description of classificationByRank
This table organizes the data by taxonomic ranks. It takes only the top 3 taxons (judged by protein counts) for each of the different ranks (phyla,class,etc) from `Data/stats`.  **_NOTE: the top guess from this table usually agrees with "classificationByProtein" but may not, in which case you should go with classificationByProtein._**  This table was used in the creation of "finalSummary" and may not be of much use to you on its own.

##### columns are:
1) taxon rank  
2) taxon name  
3) total proteins that had a hit in NR database  
4) total proteins called by prodigal
5) percentage of protein hits to annotated proteins  
6) full taxon path  

#### Description of Plots/*.png
There is a histogram for each taxonomic rank showing the number of proteins that hit various taxa.  These figures are usefull for detecting contaminant organisms.

## Another example of output
This example shows a novel species but was well represented at the genus level.  The species may be novel but it is most certainly a Polynucleobacter(genus).  This was determined by looking at where "top_hits" or "percent" drops off.

```
### Best guess of what query is
Polynucleobacter wuianus|Polynucleobacter|Burkholderiaceae|Burkholderiales|Betaproteobacteria|Proteobacteria|Bacteria

total proteins predicted by prodigal: 106
total proteins that hit database: 101
total proteins that hit top taxa: 16

### breakdown by rank ###
# rank            name                        top_hits    db_hits    percent
# -----------------------------------------------------------------------------
species         Polynucleobacter wuianus          16        101     15.84%
genus           Polynucleobacter                  92        101     91.09%
family          Burkholderiaceae                  92        101     91.09%
order           Burkholderiales                   92        101     91.09%
class           Betaproteobacteria               101        101    100.00%
phyla           Proteobacteria                   101        101    100.00%
superkingdom    Bacteria                         101        101    100.00%
```

## Example of GC content of the contigs and color coated by genus
There is a plot for each taxon level.  This is a good way to identify contamination.
Note that  

Total=means all contigs.  
Unassigned=contigs that hit something in database but had no taxonomy.  
Nohit=no database hit at all.  

![Example GC plot at genus level](contigs_Genus.png?raw=true)