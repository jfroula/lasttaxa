FROM continuumio/miniconda

# install dependencies
RUN conda install -y -c bioconda last=941 
RUN conda install -y -c bioconda prodigal=2.6.3
RUN conda install -y -c bioconda bbmap=37.02
RUN conda install -y -c bioconda gnuplot-py=1.8
RUN conda install -y -c auto db-sqlite3=0.0.1

# perl modules
RUN conda install -y -c bioconda perl-tie-ixhash=1.23
RUN conda install -y -c bioconda perl-bioperl
RUN conda install -y -c bioconda perl-dbd-sqlite=1.50 

# this makes gnuplot_py work
#RUN apt-get install -y libjpeg8

COPY bin/ /usr/local/bin/
