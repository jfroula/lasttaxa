#!/usr/bin/env perl

use strict;
use warnings;
use Getopt::Long;
use DBI;
use FileHandle;
use Cwd;
use Tie::IxHash;
use File::Basename;
use FindBin qw($RealBin);
use lib $RealBin;
use DBLookup;

#
# S.Trong - 08/22/2012; rewrite of James Han's whatdatax.pl code.
#

my ($o_header_only, $o_tax_only, $o_db_path, $o_output_dir, $o_fill, $o_strain, $o_header_full);
my $progName = basename($0);
my $usage =<<"USAGE";
    $progName [options] <blast tophit file> <contig/read fasta>

    options
    -od             output dir (optional).
    -tax_only       only output read to tax mapping (optional). if this option is
                    used, do not include the <contig/read fasta> argument.
    -fill           add species name derived from tophit file if tax for genus is found but not species.
    -header_only    only output read headers instead of headers and reads (optional).
    -header_full    use full header name from fasta file to match with tophit file (optional; default uses
                    the first non-whitespace word as the header name in the fasta to match the query in
                    the tophit file).
    -strain         if strain is found in name, use this to lookup tax (optional; default=use genus species only)
    -dbpath         path to sqlite db containing tax info (optional; alternatively you can set the environment 
                    variable TAXMAPPER_DB_PATH to the sqlite db path).

USAGE

if (!GetOptions(
            'od=s'           =>    \$o_output_dir,
            'tax_only'       =>    \$o_tax_only,
            'fill'           =>    \$o_fill,
            'strain'         =>    \$o_strain,
            'dbpath=s'       =>    \$o_db_path,
            'header_only'    =>    \$o_header_only,
            'header_full'    =>    \$o_header_full,
)) {
    print STDERR $usage;
    exit;
}

if ( @ARGV < 2 ) {
    print STDERR $usage;
    exit;
}

my ($blast_top_hits, $fasta_file) = @ARGV; 
my $output_dir = defined $o_output_dir ? $o_output_dir : getcwd(); 
my $accession_table = "accNums.tax";  # produced by acc2whole_taxonomy.pl

if ( !-e $accession_table ) {
    print STDERR "Cannot find file $accession_table\n";
    exit;
}
if ( !-e $blast_top_hits ) {
    print STDERR "Cannot find file $blast_top_hits\n";
    exit;
}
if ($fasta_file){
    if ( !-e $fasta_file ) {
        print STDERR "Cannot find file $fasta_file\n";
        exit;
    }
}

if (!defined $o_tax_only) {
    if ( !defined $fasta_file || !-e $fasta_file ) {
        print STDERR "Cannot find file $fasta_file\n";
        exit;
    }
    mkdir($output_dir) unless -d $output_dir;
}

# 
# Load blast results
#
my %blast_results;
my %genSpecs = ();
my %taxlist = ();
my %read_to_tax = ();
my %acc2read_hash = ();
my @taxLevels = qw(Kingdom Phylum Class Order Family Genus Species);
my $maxFileHandle = 1000;
my $dbObj = DBLookup->new($o_db_path);

print "Mapping blast entries to tax levels ...\n" if !$o_tax_only;
#
# open the top blast hits file to get the read names
# which are used later to grab the protein sequences from the fasta
# 
open(my $acc_fh, $blast_top_hits) or die $!;
while(my $entry = <$acc_fh>){
    chomp $entry;
    next if ( !length($entry) || $entry =~ /^#/ );
    my $accession;
    my ($queryName, $subjectName) = (split/\t/, $entry)[0,1];

    # extract accession from line whether its
    # old NCBI NR format or new.
    if ($subjectName =~ /^gi\|\d+\|.*?\|(.*?)\|/){
        $accession = $1;
    }else{
        $accession = $subjectName;
    }
    $acc2read_hash{$accession} = $queryName;
}
close $acc_fh;

#
# open the accNums.tax file to get the species names
# 
open(my $tophit_fh, $accession_table) or die $!;
while(my $entry = <$tophit_fh>){
    next if ( !length($entry) || $entry =~ /^#/ );
    my ($accession, $taxID, $subjectName) = (split/\t/, $entry);

    die "failed to find $accession in acc2read_hash\n" unless exists $acc2read_hash{$accession};
        
    my @taxline = split(/\|/,$subjectName);
    my $genusSpecies = shift @taxline;; 
    next if !length $genusSpecies;
    
    if ( !defined $taxlist{$genusSpecies } ) {
        my @taxlvls = $dbObj->whatdatax($genusSpecies );
        $taxlist{$genusSpecies } = \@taxlvls if @taxlvls;
    }
    
    # If the tophit file is generated from blasting a casava 1.8+ fastq file,
    # the read pairing information is lost. The fasta header is set here may
    # not truly reflect the fasta entry name if the input file. Will have to
    # handle this case in the subsequent code.
    #
    my $fasta_header = parseFastaHeader($acc2read_hash{$accession});
    $read_to_tax{$fasta_header} = $genusSpecies;
}
close $tophit_fh;

if ( $o_tax_only ) {
    foreach my $header (keys %read_to_tax) {
        my $genusSpecies  = defined $read_to_tax{$header} ? $read_to_tax{$header} : '';
        my $taxlist = defined ${$taxlist{$genusSpecies }}[0] ?
            ${$taxlist{$genusSpecies }}[0] : '';
        print "$header\t$taxlist\t$genusSpecies \n";
    }
    exit;
}

print "Mapping fasta entries to tax levels ...\n";

my %reads = ();
my $readData = '';
my $header = '';

open(my $fasta_fh, $fasta_file) or die $!;

my $lastHeader = '';
while (my $fasta_entry = <$fasta_fh>) {
    chomp $fasta_entry;
    next if !length $fasta_entry;
    if ($fasta_entry =~ /^>/) {
        $header = parseFastaHeader($fasta_entry);
        $reads{$lastHeader} = $readData if length $lastHeader;
        $readData = '';
        $lastHeader = $header;
        
        # If the tophit file is generated from blasting a casava 1.8+ fastq file,
        # the read pairing information is lost. This will cause a discrepency
        # betwwen the header name in the tophit file and the input fasta file.
        # Need to compensate for this by stripping the read pairining info from
        # the input file to match that in the tophit file.
        #
        my $modHeader = $header;
        $modHeader =~ s/\s+.+$// if !$o_header_full;
        
        if ( defined $read_to_tax{$modHeader} ) {
            my $genusSpecies  = $read_to_tax{$modHeader};
            if ( $genusSpecies  && defined $taxlist{$genusSpecies } ) {
                my @tax = @{$taxlist{$genusSpecies }};
                chomp $tax[0];
                $tax[0] =~ s/\.//g;
                my $idx = 0;
                my @names = split /;/, $tax[0];
                foreach my $taxLevel (@taxLevels) {
                    next if $idx > $#taxLevels;
                    my $taxLevel = $taxLevels[$idx];
                    my $name = $idx <= $#names ? $names[$idx] : '';
                       $name = 'Unassigned' if !$name;
                    $blast_results{$header}{$taxLevel} = $dbObj->mkCharsFriendly($name);
                    $idx++;
                }
            } else {
                foreach my $taxLevel (@taxLevels) {
                    $blast_results{$header}{$taxLevel} = 'Unassigned';
                }
            }
        } else {
            foreach my $taxLevel (@taxLevels) {
                $blast_results{$header}{$taxLevel} = 'Nohit';
            }
        }
    } else {
        $readData .= $fasta_entry;
    }
}
$reads{$lastHeader} = $readData;

close $fasta_fh;

# categorize
my %stats;
tie my %outfile_fhs, "Tie::IxHash";
my $outfile_fh;
my $numFileHandles = 0;
my %filesCreated = ();

print "Creating tax level fasta files ...\n";

foreach my $header (keys %blast_results) {
    my $modHeader = $header;
       $modHeader =~ s/\s+/\_/g;
    foreach my $lev (keys %{$blast_results{$header}}) {
        my $output_filename = '';
        my $taxname = $blast_results{$header}{$lev};
        
        $output_filename = "$lev.$taxname";
        $stats{$lev}{$taxname}++;
        
        if( $o_header_only ){
            $output_filename .= '.txt';
        } else {
            $output_filename .= '.fasta';
        }
        
        # Here, we store an open filehandle for each filename. If an opened
        # filehandle exist for the filename, we use it. Otherwise, we create
        # a new one and store it in a hash.
        #
        # Get filehandle.
        # Store filehandle if newly created. Otherwise, retrieve filehandle
        # from hash. Note that if number of filehandles opened exceed
        # $maxFileHandle, an error occurs (when using FileHandle module). To
        # remedy this, remove first stored filehandle from hash when this
        # threshold is hit.
        #
        if ( defined $outfile_fhs{$output_filename} ) {
            $outfile_fh = $outfile_fhs{$output_filename};
        } else {
            if ( $numFileHandles >= $maxFileHandle && %outfile_fhs ) {
                my ($name, $fh) = each %outfile_fhs;
                $fh->close;
                delete $outfile_fhs{$name};
                $numFileHandles--;
                
                # because we've deleted an element from %outfile_fhs, we need to
                # reset the internal cursor for the iterator when using the
                # "each %outfile_fhs" function. The line below is intended to
                # reset the cursor.
                #
                keys %outfile_fhs;
                    
            }
            $numFileHandles++;
            
            # Check if file already exists yet we haven't created this file before,
            # (meaning file already exist before running this script). If so,
            # need to remove the file.
            #
            if ( !$filesCreated{$output_filename} && -s
                "$output_dir/$output_filename" ) {
                unlink ("$output_dir/$output_filename");
            }
                
            # Create new filehandle for each filename and store in hash. This
            # will reduce the operation of open/closing files multiple times.
            #
            $outfile_fh = FileHandle->new(">>$output_dir/$output_filename");
            if ( !defined $outfile_fh ) {
                print STDERR "ERROR: failed to create filehandle for $output_filename.\n";
                exit 1;
            }
            $outfile_fhs{$output_filename} = $outfile_fh;
            $filesCreated{$output_filename}++;
        }
        print $outfile_fh ">" unless $o_header_only;
        print $outfile_fh "$modHeader\n";
        print $outfile_fh "$reads{$header}\n" unless $o_header_only;
    }        
}

# close all filehandles.
#
foreach my $file (keys %outfile_fhs) {
    close $outfile_fhs{$file};
}

#print stats
open(my $stats_fh, '>', "$output_dir/stats.txt") or die $!;
foreach my $taxlev (@taxLevels){
    foreach my $taxname (sort keys %{$stats{$taxlev}}){
        my $count = $stats{$taxlev}{$taxname};
        my $taxlvl = join ",", $dbObj->whatdatax($taxname);
        $taxlvl =~ s/,\s*$//;
        print $stats_fh "$taxlev\t$taxname\t$count\t$taxlvl\n";
    }
}
close($stats_fh) or die $!;

exit 0;

#=========================================================================
sub parseFastaHeader {
    my $header = shift;
    
    my $name = $header;
    if (!defined $o_header_full && $header =~ /^>(\S+)(\s+\S+)*/) {
        $name = $1;
    } elsif ($header =~ /^>(.+)/) {
        $name = $1;
    }
    
    return $name;
}
    
#=========================================================================
