#!/bin/bash

function processInput() {
    # gather up input from STDIN, ARGV or FILE then run __cmd over it
    # __cmd is a function which must be defined in the caller
    local ARG
    declare -Ff __cmd 2>/dev/null 1>/dev/null || ( echo 1>&2 "(processInput) Need to define __cmd " ;return; )
    if [ $# -eq 0 ] || [ "x$1" = "x-" ]  ; then
        # read from stdin
        sort - | __cmd ;
    else
        for ARG in $*
        do

            if [ -s $ARG ] ; then
                #    echo "# read from file"
               cat $ARG | sort | __cmd  ;

            # read from ARGV
            elif [ "x$ARG" != "x" ] ; then
                #echo "# read from argv"
               echo $ARG | perl -pne 's/\s+/\n/g' | sort | __cmd ;

            else
                    echo "(processInput) Invalid argument: '$ARG' is neither a string, file, nor stdin" ; return ;
            fi
        done
    fi
        unset -f __cmd ;
}


function tophit() {
        #  1      2      3      4       5    6    7        8       9      10        11     12      13       14
        # query subject score expect length %id q_start q_stop q_length q_strand s_start s_stop s_length s_strand
        #
        local TAB=$(echo -e "\t") ;
        function __cmd() {
            sort -t"$TAB" -k1,1 -k3,3n | \
            nawk -F'[\t]' '!/#/{
                    s=gensub(" ","  ",1,$2); # creates new column:=3
                    t=gensub(" ","_","g",s)
                    a[$1]=t" "$4" "$5" "$6" "$9" "$13}
                    END{ for(e in a) print e,a[e] }' | sort -k 4,4n -k 6,6n  ;
        }
        processInput $* ;
}

function countIt() {
    local FLD=${1:?Specify one more more fields (1-based) to count} ;
    shift; # leave only file or stream in $*
    # combining more than one col into key is also doable
    perl -ane 'BEGIN{$e=shift;$e--}
        next if /^\s*$|^#/;
        $h{$F[$e]}++;
        END{print "$_ $h{$_}\n" foreach (sort keys %h)}' $FLD $* | sort -k 2,2n ;
}

SCRIPT=$(readlink -f $0)
SCRIPT_PATH=`dirname $SCRIPT`

#source "$SCRIPT_PATH/qctools.sh"

BFILE=${1:?"Specify blast results file"}
if [ ! -s $BFILE ] ; then echo "can't find $BFILE" ; exit ; fi

# 
TMPF=$(mktemp)
SUMMARY=$BFILE.tophit.summary

# tophit blast file
tophit $BFILE > $BFILE.tophit

# extract ref names and summarize
#$PERL -ne 'print "$2\n" if /(\S+)__(\S+)/' $BFILE.tophit | $NAWK -F'[_ ]' '{print $1"_"$2}'| countIt 1 > $SUMMARY
perl -ne 'print "$2\n" if /(\S+)__(\S+)/' $BFILE.tophit | nawk -F'[;_ ]' '{print $1"_"$2}'| countIt 1 > $SUMMARY

#$CUT -d' ' -f1 $SUMMARY |  xargs -0 $SCRIPT_PATH/tax2list.sh  | $SORT >> $TMPF
#$JOIN -1 1 -2 1 <($SORT -k1,1 $SUMMARY) <($SORT -k1,1 $TMPF) >  $BFILE.taxlist 
rm $TMPF
