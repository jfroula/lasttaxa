package JGI::QDExtension::Histogram;

=head1 NAME

JGI::QDExtension::Histogram
QD extension module to compute histogram data from a given dataset.

=head1 VERSION

$Revision: 1.13 $

$Date: 2010-05-06 21:44:23 $

=head1 SYNOPSIS

=head1 DESCRIPTION

Computes the histogram statistics and bin information for a given dataset.
The dataset could be passed in from either an array containing each observation
or a hash where the key=number of observations, value=observation.

=head1 AUTHOR(S)

Stephan Trong

=head1 COPYRIGHT

DOE Joint Genome Institute Microbial Genomics

Copyright (C) 2005 The Regents of the University of California
All rights reserved.

NOTICE: The Government is granted for itself and others acting on its
behalf a paid-up, nonexclusive irrevocable worldwide license in this
data to reproduce, prepare derivative works, and perform publicly and
display publicly. Beginning five (5) years after permission to assert
copyright is granted, subject to two possible five year renewals, the
Government is granted for itself and others acting on its behalf a
paid-up, non-exclusive, irrevocable worldwide license in this data to
reproduce, prepare derivative works, distribute copies to the public,
perform publicly and display publicly, and to permit others to do so.
NEITHER THE UNITED STATES NOR THE UNITED STATES DEPARTMENT OF ENERGY,
NOR ANY OF THEIR EMPLOYEES, MAKES ANY WARRANTY, EXPRESS OR IMPLIED,
OR ASSUMES ANY LEGAL LIABILITY OR RESPONSIBILITY FOR THE ACCURACY,
COMPLETENESS, OR USEFULNESS OF ANY INFORMATION, APPARATUS, PRODUCT,
OR PROCESS DISCLOSED, OR REPRESENTS THAT ITS USE WOULD NOT INFRINGE
PRIVATELY OWNED RIGHTS.

=head1 HISTORY

=over

=item *

S.Trong 12/19/2005 Creation

=back

=cut

#============================================================================#

use strict;
use File::Basename;
use POSIX qw(floor);
use FindBin;
use lib "$FindBin::Bin/../../../lib";

#============================================================================#
sub new {

    my $class = shift;
    
    my $self = { initialBinSize => 0,
                 minObservation => undef,
                 maxObservation => undef,
                 showEmptyBins  => 0,
               };
    
    bless $self, $class;
    return $self;
}

#============================================================================#
sub setInitialBinSize {
    my $self = shift;
    my $value = shift;
    $self->{initialBinSize} = $value;
    
}

#============================================================================#
sub setMinObservation {
    my $self = shift;
    my $value = shift;
    $self->{minObservation} = $value;
    
}

#============================================================================#
sub setMaxObservation {
    my $self = shift;
    my $value = shift;
    $self->{maxObservation} = $value;
    
}

#============================================================================#
sub showEmptyBins {
    my $self = shift;
    my $value = shift || 1;
    
    $self->{showEmptyBins} = $value;
    
}

#============================================================================#
sub computeHistogram {
    
    my $self = shift;
    my $dataPointsHashOrList = shift;
    my $binWidth = shift;
    my $initialMinBin = shift;
    
    $initialMinBin = $self->{initialBinSize} if !defined $initialMinBin;
    
    # $dataPointsHashOrList is either a reference to an array containing
    # the observations or a reference to a hash where the key=observation,
    # and the value=quantity of the observation.
    
    my $sumOfDatapoints = 0;
    my $sumOfSquaredValues = 0; 
    my %histogram = ();
    my %discriminators = ();
    my $bounded = 0;
    my $maxBin = $initialMinBin+$binWidth;
    my $minBin;
    my %histogramAttribs = ();

    my $maxBinQuantity = 0;
    my $minBinQuantity = 0;
    my $numberOfDatapoints = 0;
    my $entropy = 0;
    my $sumOfQuantity = 0;
    my $median = 0;
    my %dataPointsHash = ();

    # If $dataPointsHashOrList is a reference to an array, convert to hash
    # where key=observation, value=quantity.
    #
    if ( ref($dataPointsHashOrList) eq 'ARRAY' ) {
        %dataPointsHash = _convertDataPointListToHash( @$dataPointsHashOrList );
    } else {
        %dataPointsHash = %$dataPointsHashOrList;
    }
    
    # Compute number of datapoints.
    #
    foreach my $observation ( sort {$a<=>$b} keys %dataPointsHash ) {
        last if ( defined $self->{minObservation} && $observation <
            $self->{minObservation} );
        last if ( defined $self->{maxObservation} && $observation >
            $self->{maxObservation} );
        
        my $quantity = $dataPointsHash{$observation};
        if ($numberOfDatapoints == 0) {
            my ($binStart, $binStop) = _computeBinStartAndStop($observation,
                $initialMinBin, $binWidth );
            $maxBin = $binStart;
            $maxBinQuantity = $quantity;
            $minBinQuantity = $quantity;
        }
        $numberOfDatapoints += $quantity;
    }
   
    my %binNumber = ();
    
    # Bin observations to create histogram.
    #
    foreach my $observation ( sort {$a<=>$b} keys %dataPointsHash ) {
        
        last if ( defined $self->{minObservation} && $observation <
            $self->{minObservation} );
        last if ( defined $self->{maxObservation} && $observation >
            $self->{maxObservation} );
        
        my $quantity = $dataPointsHash{$observation};
        
        # determin min and max bin and quantity.
        my ($binStart, $binStop) = _computeBinStartAndStop($observation,
            $initialMinBin, $binWidth );
        
        $binNumber{$binStart} += $quantity;

        # set max and min bin.
        $minBin = $binStart if !defined $minBin;   
        $minBin = $binStart if $binStart < $minBin;        
        $maxBin = $binStart if $binStart > $maxBin;
        
        # set max and min bin quantity.
        $maxBinQuantity = $binNumber{$binStart}
            if $binNumber{$binStart} > $maxBinQuantity;
            
        $minBinQuantity = $binNumber{$binStart}
            if $binNumber{$binStart} < $minBinQuantity;

        # determine sums.
        $sumOfDatapoints += $observation*$quantity;
        $sumOfSquaredValues += $quantity*$observation*$observation;
        $sumOfQuantity += $quantity;

        # compute median.
        if ( $sumOfQuantity-$quantity < ($numberOfDatapoints/2) &&
             ($numberOfDatapoints/2) <= $sumOfQuantity ) {
            $median = $observation;
        }

    }
    
    # Set $minBin if it is undefined.  $minBin is undefined if there are no
    # observations. This is the minimum bin value.
    #
    $minBin = $initialMinBin if !defined $minBin;   

    my @binNumbers = sort {$a <=> $b} keys %binNumber;
    if ( $self->{showEmptyBins} ) {
        my $min = $binNumbers[0];
        my $max = $binNumbers[$#binNumbers];
        # Expand bins starting with min bin, stepping $binWidth to $max bin.
        #
        @binNumbers = ();
        for (my $val=$min; $val<=$max; $val+=$binWidth) {
            push @binNumbers, $val;
        }
    }
        
    # Compute entropy.
    #
    foreach my $binStart ( @binNumbers ) {
        next if !defined $binNumber{$binStart};
        my $quantity = $binNumber{$binStart};
        my $p = $numberOfDatapoints ? $quantity/$numberOfDatapoints : 0;
        if ($p > 0) {
            $entropy -= $p*log($p)/log(2);
        }
    }    
    
    # Compute mean and std dev.
    #
    my $mean = 0;
    my $meanStdDev = 0;
    if ( $numberOfDatapoints > 0 ) {
        $mean = $sumOfDatapoints/$numberOfDatapoints;
    }
    if ( $numberOfDatapoints > 1 ) {
        $meanStdDev = sqrt( 
                          ($sumOfSquaredValues - 
                           $sumOfDatapoints*$sumOfDatapoints/$numberOfDatapoints)/
                          ($numberOfDatapoints-1)
                         );
    }

    # Store histogram statistics.
    #
    $histogramAttribs{numberOfDatapoints} = $numberOfDatapoints;
    $histogramAttribs{sumOfDatapoints} = $sumOfDatapoints;
    $histogramAttribs{mean} = $mean;
    $histogramAttribs{meanStdDev} = $meanStdDev;
    $histogramAttribs{median} = $median;
    $histogramAttribs{entropy} = $entropy;
    $histogramAttribs{minBin} = $minBin;
    $histogramAttribs{maxBin} = $maxBin;
    $histogramAttribs{binWidth} = $binWidth;
    $histogramAttribs{minBinQuantity} = $minBinQuantity;
    $histogramAttribs{maxBinQuantity} = $maxBinQuantity;
    $histogramAttribs{initialMinBin} = $initialMinBin;

    my $runningTotal = 0;
    my $maxFraction = 0;
    
    foreach my $binStart ( @binNumbers ) {
        my $binStop = $binStart + $binWidth;
        my $quantity = defined $binNumber{$binStart} ? $binNumber{$binStart}:0;
        $runningTotal += $quantity;
        
        my $cumulativePercent = $numberOfDatapoints ?
            $runningTotal/$numberOfDatapoints*100 : 0;
        my $fraction = $numberOfDatapoints ?
            $quantity/$numberOfDatapoints*100 : 0;
        my $isMean = _isValueWithinRange($mean, $binStart, $binStop);
        my $isMedian = _isValueWithinRange($median, $binStart, $binStop);
        
        my $isMinBinQuantity = $quantity == $minBinQuantity ? 1:0;
        my $isMaxBinQuantity = $quantity == $maxBinQuantity ? 1:0;
        my $isMaxBin = $binStart == $maxBin ? 1:0;
        my $isMinBin = $binStart == $minBin ? 1:0;
        
        my %binAttribs = (
            binStart=>$binStart,
            binStop=>$binStop,
            quantity=>$quantity,
            fraction=>$fraction,
            cumulativeFraction=>$cumulativePercent,
            runningTotal=>$runningTotal,
            isMean=>$isMean,
            isMedian=>$isMedian,
            isMinBinQuantity=>$isMinBinQuantity,
            isMaxBinQuantity=>$isMaxBinQuantity,
            isMaxBin=>$isMaxBin,
            isMinBin=>$isMinBin,
            #binValue=>0,
        );
        push @{$histogramAttribs{bins}}, \%binAttribs;
        $maxFraction = $fraction if $fraction > $maxFraction;
    }
    
    $histogramAttribs{maxFraction} = $maxFraction;
    
    $self->{_histogramAttribs} = \%histogramAttribs;
    
    return %histogramAttribs;

}

#============================================================================#
sub _convertDataPointListToHash {
    
    my @dataPoints = @_;
    
    my %dataPointsHash = ();
    
    foreach my $data (@dataPoints) {
        $dataPointsHash{$data}++;
    }
    
    return %dataPointsHash;

}

#============================================================================#
sub _isValueWithinRange {
    
    my $valueToTest = shift;
    my $binStart = shift;
    my $binStop  = shift;

    my $returnValue = 0;
    
    if ( $binStart <= $valueToTest && $valueToTest < $binStop ) {
        $returnValue = 1;
    }
    
    return $returnValue;
    
}

#============================================================================#
sub _computeBinStartAndStop {
    
    my $observation = shift;
    my $minBin = shift;
    my $binWidth = shift;
    
    my $binNumber = 0;
    my $binStart = 0;
    my $binStop = 0;
    
    if ( $binWidth > 0 ) {
        $binNumber = floor( ($observation-$minBin)/$binWidth );
        $binStart = $minBin + $binNumber*$binWidth;
        $binStop = $binStart + $binWidth;
    } else {
        $binStart = $observation;
        $binStop = $observation;
    }
    
    return $binStart, $binStop;
    
}

#============================================================================#
sub getHistogramPicture {

    my $self = shift;
    my %params = @_;
    
    # %params can be:
    # format=>'csv' - display histogram as tab delimited values.
    # format=>'detailed' - display detailed histogram.
    # noHeader=>1 - set this to not print header info.
    
    my %histogramAttribs = %{$self->{_histogramAttribs}};
    my $pictureFormat = defined $params{format} ? $params{format} : 'detailed';
    my $noHeader = defined $params{noHeader} ? $params{noHeader} : 0;

    my $numberOfDatapoints = $histogramAttribs{numberOfDatapoints};
    my $sumOfDatapoints = $histogramAttribs{sumOfDatapoints};
    my $mean = $histogramAttribs{mean};
    my $meanStdDev = $histogramAttribs{meanStdDev};
    my $median = $histogramAttribs{median};
    my $entropy = $histogramAttribs{entropy};
    my $minBin = $histogramAttribs{minBin};
    my $maxBin = $histogramAttribs{maxBin};
    my $binWidth = $histogramAttribs{binWidth};
    my $minBinQuantity = $histogramAttribs{minBinQuantity};
    my $maxBinQuantity = $histogramAttribs{maxBinQuantity};

    my $runningTotal = 0;
    my $lastBinOccupied = 0;
    my $min;
    my $max;
    my $histogramPicture = "";
    
    if ( !$noHeader ) {
        $histogramPicture =
        sprintf("# Number of datapoints: %d\n",$numberOfDatapoints).
        sprintf("# Sum of datapoints: %d\n",$sumOfDatapoints). 
        sprintf("# Mean (weighted): %.2f +/- %.2f\n",$mean,$meanStdDev).
        sprintf("# Median: %.2f\n",$median).
        sprintf("# Bin width: %.2f\n",$binWidth).
        sprintf("# Min bin: %.2f\n",$minBin).
        sprintf("# Max bin: %.2f\n",$maxBin).
        sprintf("# Min bin quantity: %.2f\n",$minBinQuantity).
        sprintf("# Max bin quantity: %.2f\n",$maxBinQuantity);
    }
    
    if ( $pictureFormat eq 'detailed' && !$noHeader ) {
        $histogramPicture .=
        "# Histogram format: <bar> <binStart> - <binEnd> : [ quantity pct cumPct runningTotal ]\n";
        
    } elsif ( $pictureFormat eq 'csv' && !$noHeader ) {
        $histogramPicture .=
        "# Bin\tQuantity\tPct\tRunningPct\tRunningTotal\n";
    }

    my @binAttribs = @{$histogramAttribs{bins}};
    
    # sort binAttribs by binStart.
    my %binStartPositions = ();
    foreach my $ref ( @binAttribs ) {
        my $binStart = $ref->{binStart};
        $binStartPositions{$binStart} = $ref;
    }
    
    my $lastBinStart = $minBin;
    my $lastBinAttribs;
    my $lastBinStop;
    
    foreach my $binStart ( sort {$a <=> $b} keys %binStartPositions ) {
        
        if ( defined $lastBinAttribs ) {
            $histogramPicture .= _addToHistogramPicture( \%histogramAttribs,
                $lastBinAttribs, $binStart + $binWidth - 1, $pictureFormat );
            if ( $binWidth && $pictureFormat eq 'detailed' && $lastBinStart + $binWidth < $binStart ) {
                $histogramPicture .= "|...\n";
            }
        }
        $lastBinAttribs = $binStartPositions{$binStart};
        $lastBinStart = $binStart;
        $lastBinStop = $binStart + $binWidth;
    }
    
    $histogramPicture .= _addToHistogramPicture( \%histogramAttribs,
        $lastBinAttribs, $lastBinStop, $pictureFormat );

    return $histogramPicture;

}

#============================================================================#
sub printHistogram {
    
    my $self = shift;
    my %params = @_;
    
    print $self->getHistogramPicture(%params);

}

#============================================================================#
sub _addToHistogramPicture {

    my $H_histogramAttribs = shift;
    my $H_binAttribs = shift;
    my $lastBinStop = shift;
    my $pictureFormat = shift;

    my $initialMinBin = $H_histogramAttribs->{initialMinBin};
    my $numDataPoints = $H_histogramAttribs->{numberOfDatapoints};
    my $minBinQuantity = $H_histogramAttribs->{minBinQuantity};
    my $maxBinQuantity = $H_histogramAttribs->{maxBinQuantity};
    my $quantity = $H_binAttribs->{quantity};
    my $binStart = $H_binAttribs->{binStart};
    my $binStop = $H_binAttribs->{binStop};
    my $fraction = $H_binAttribs->{fraction};
    my $cumulFrac = $H_binAttribs->{cumulativeFraction};
    my $runningTotal = $H_binAttribs->{runningTotal};
    my $minBin = $H_histogramAttribs->{minBin};
    my $maxBin = $H_histogramAttribs->{maxBin};
    my $maxFraction = sprintf("%.1f", $H_histogramAttribs->{maxFraction});
    my $mean = $H_histogramAttribs->{mean};
    my $median = $H_histogramAttribs->{median};
    my $binWidth = $H_histogramAttribs->{binWidth};
    
    my $result = "";
    my $sformat = "%-".length($maxBinQuantity)."d %-".
        length($maxFraction)."s %-5s %-".length($numDataPoints)."s";
    my $blen = length($maxBin) - length($binStart) + length($binStart);
    my $bformat = $binWidth ?
        "%${blen}s - %-".length($maxBin)."s" :
        "%${blen}s - %-".length($minBin)."s";
        
    if ( $pictureFormat eq 'detailed' ) {
        my $picture = $maxBinQuantity ?
            "|" . "X" x sprintf("%.0f",40*$quantity/$maxBinQuantity) :
            "|";
        my $space = $maxBinQuantity ?
            " " x sprintf("%.0f",40*(1-$quantity/$maxBinQuantity)) :
            " ";
            
        $fraction = sprintf("%.1f", $fraction);
        $cumulFrac = sprintf("%.1f",$cumulFrac);
        $maxFraction = sprintf("%.1f", $maxFraction);
        
        $result .= "$picture$space ";
        $result .= $binStart == $binStop ? 
            sprintf("$bformat : ",$binStart, $lastBinStop) :
            sprintf("$bformat : ",$binStart, $binStop);
        $result .= sprintf( "[ $sformat ]",$quantity, $fraction, $cumulFrac, $runningTotal);
        $result .= " mean" if ($mean >= $binStart && $mean < $binStop);
        $result .= " median" if ($median >= $binStart && $median < $binStop);
        $result .= " min" if $H_binAttribs->{isMinBinQuantity};
        $result .= " max" if $H_binAttribs->{isMaxBinQuantity};
        $result .= "\n";
        
    } elsif ( $pictureFormat eq 'csv' ) {
        $result .= "$binStart\t$quantity\t$fraction\t$cumulFrac\t$runningTotal\n";
    }
    
    return $result;

}

#============================================================================#
sub getBinAttribs {
    
    my $self = shift;
    
    my %histogramAttribs = %{$self->{_histogramAttribs}};
    my $numberOfDatapoints = $histogramAttribs{numberOfDatapoints};
    my $sumOfDatapoints = $histogramAttribs{sumOfDatapoints};
    my $mean = $histogramAttribs{mean};
    my $meanStdDev = $histogramAttribs{meanStdDev};
    my $median = $histogramAttribs{median};
    my $entropy = $histogramAttribs{entropy};
    my $minBin = $histogramAttribs{minBin};
    my $maxBin = $histogramAttribs{maxBin};
    my $binWidth = $histogramAttribs{binWidth};
    my $minBinQuantity = $histogramAttribs{minBinQuantity};
    my $maxBinQuantity = $histogramAttribs{maxBinQuantity};

    my $runningTotal = 0;
    my $lastBinOccupied = 0;
    my $min;
    my $max;
    my $histogramPicture = "";
    my @results = ();
    
    my @binAttribs = @{$histogramAttribs{bins}};
    
    my %binStartPositions = ();
    my $previousBinStart = $minBin;
    
    foreach my $refBinAttrib ( sort {$a->{binStart} <=> $b->{binStart}} @binAttribs ) {
        my $binStart = $$refBinAttrib{binStart};
        my $binStop = $$refBinAttrib{binStop};
        my $quantity = $$refBinAttrib{quantity};
        my $percent = $$refBinAttrib{fraction};
        my $cumPercent = $$refBinAttrib{cumulativeFraction};
        my $runningTotal = $$refBinAttrib{runningTotal};
        my $bar = '';
        my $bin = $binStart;
    
        unless ( $binStart == $minBin || $binStart == $previousBinStart + $binWidth) {
            $histogramPicture = "|...\n";
            $bar = $histogramPicture;
        } else {
            my $quantPct = $maxBinQuantity ? $quantity/$maxBinQuantity : 0;
            my $picture = "|" . "X" x sprintf("%.0f",40*$quantPct);
            my $space = " " x sprintf("%.0f",40*(1-$quantPct));
            my $result = "$picture$space ";
            $bar = "$picture$space";
            $bin= $binStart;
            $histogramPicture = $result;
        }
        
        push @results, {
            picture=>$histogramPicture, # |xxxx binStart < $binStop
            bar=>$bar,                  # |xxxx
            bin=>$bin,
            quantity=>$quantity,
            percent=>$percent,
            cumPercent=>$cumPercent,
            runningTotal=>$runningTotal,
            isMean=>$$refBinAttrib{isMean},
            isMedian=>$$refBinAttrib{isMedian},
            isMin=>$$refBinAttrib{isMinBinQuantity},
            isMax=>$$refBinAttrib{isMaxBinQuantity}
        };
            
        $previousBinStart = $binStart;
    }
    
    return @results;
    
}

#============================================================================#
1;
