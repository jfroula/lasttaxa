package DBLookup;

use strict;
use warnings;
use DBI;
use Cwd;
use Data::Dumper;
use FindBin qw($RealBin);

#============================================================================#
sub new {
    my $class = shift;
    my $usrDbPath = shift || '';
    my $dbPath;

    if ($usrDbPath) {
        $dbPath = $usrDbPath;
    } elsif (defined $ENV{TAXMAPPER_DB_PATH}) {
        $dbPath = $ENV{TAXMAPPER_DB_PATH};
    }
    my $dbNames = "$dbPath/names.db";
    my $dbNodes = "$dbPath/nodes.db";
    my $dbhNames = DBI->connect("dbi:SQLite:$dbNames", "", "", {RaiseError => 1, AutoCommit => 0});
    my $dbhNodes = DBI->connect("dbi:SQLite:$dbNodes", "", "", {RaiseError => 1, AutoCommit => 0});
    
    my $self = {
        dbNames=>$dbNames,
        dbNodes=>$dbNodes,
        dbhNames=>$dbhNames,
        dbhNodes=>$dbhNodes,
    };
    
    bless $self, $class;
    return $self;
}

#============================================================================#
sub _isTableFormat3digits {
    my $dbh = shift;
    my $tableCt = 0;
    my $sth = $dbh->prepare("SELECT * FROM sqlite_master WHERE type='table'");
    $sth->execute();
    while (my $row=$sth->fetchrow_arrayref()) {
        $tableCt++;
    }
    return $tableCt > 100 ? 1:0;
}

#============================================================================#
sub getScientificNameByGiNumber {
    my $self = shift;
    my $giNumber = shift;
    
    my $digitLen = $self->{isTableFormat3digits} ? 3:2;
    my $table = "gi_to_tax_".substr($giNumber, length($giNumber)-$digitLen, length($giNumber));
    my $taxid = 0;
    my $genusSpecies = '';
    my $dbhGiTax = $self->{dbhGiTax};
    my $dbhNames = $self->{dbhNames};
    
    my $sth = $dbhGiTax->prepare("SELECT taxid from $table where gi=?");
    $sth->execute($giNumber);
    while (my $row=$sth->fetchrow_arrayref()) {
        $taxid = $$row[0];
    }
    if ( $taxid ) {
        $sth = $dbhNames->prepare("SELECT name from taxtonames where taxid=?");
        $sth->execute($taxid);
        while (my $row=$sth->fetchrow_arrayref()) {
            $genusSpecies = $$row[0];
        }
    }
    
    return $genusSpecies;
}

#============================================================================#
sub getGenusSpecies {
    my $self = shift;
    my $subjectName = shift;
    my $useFullName = shift || 0;
    
    # Different types of subject name formats encountered:
    #
    # gi|85674274|dbj|AP009048.1| Escherichia coli str. K12 substr. W3110_DNA, complete_genome
    # AATY01000023.1.3315 Bacteria;Proteobacteria;Gammaproteobacteria;Vibrionales;V_ibrionaceae;Vibrio;Vibrio_cholerae_AM-19226
    # gi|489007298|ref|WP_002917876.1| gfo/Idh/MocA family oxidoreductase [Streptococcus sanguinis]<>gfo/Idh/MocA family oxidoreductase [Streptococcus sanguinis SK408]<>gfo/Idh/MocA family oxidoreductase_[Streptococcus_sanguinis_SK1058]<>gfo/Idh/MocA family oxidoreductase [Streptococcus sanguinis SK1087]
    # NODE_56_length_5352_cov_8.46762_ID_9239792 N/A
    # 836673 Escherichia coli str. W CP002185.1 4431305..4432849
    # 561644 Bifidobacterium subtile str. F395; DSM 20096 NR_029139.1
    
    my $genusSpecies = '';
    
    # If subject name like:
    # gi|489007298|ref|WP_002917876.1| gfo/Idh/MocA family oxidoreductase
    # [Streptococcus sanguinis]<>gfo/Idh/MocA family oxidoreductase
    # [Streptococcus sanguinis SK408]<>gfo/Idh/MocA family oxidoreductase
    # [Streptococcus sanguinis SK1058]<>gfo/Idh/MocA family oxidoreductase
    # [Streptococcus sanguinis SK1087]
    # get name in last square bracket.
    #
    if ( $subjectName =~ /.+\[(.+)\]/ ) {
        $genusSpecies  = $1;
        
    # If subject name like:
    # gi|170079663|ref|NC_010473.1| Escherichia coli ...
    #
    } elsif ( $subjectName =~ /^gi\|(?:[^\|]+)\|.+\|(?:[^\|]+)\|\s+(.+)/ ) {
        $genusSpecies  = $1;

    # If subject name like:
    # gi|170079663|ref|NC_010473.1|
    # ** no scientific name **
    #
    } elsif ( $subjectName =~ /^gi\|([^\|]+)\|ref\|(?:[^\|]+)\|\s*$/ ) {
        $genusSpecies = $self->getScientificNameByGiNumber($1);
    
    # If subjectName like:
    # AATY01000023.1.3315 Bacteria;Proteobacteria;Gammaproteobacteria;Vibrionales;
    # V_ibrionaceae;Vibrio;Vibrio_cholerae_AM-19226
    # get 7th entry delimited by semicolon.
    #
    } elsif ( $subjectName =~ /;(?:[^;]+);/ && $subjectName =~ /^\S+\s+(\S+)/ ) {
            my @hitNames = split /;/, $subjectName;
            if ( @hitNames >=7 ) {
                $genusSpecies  = $hitNames[6];
            } else {
                $genusSpecies  = $hitNames[$#hitNames];
            }

    # If subjectName like:
    # 561644 Bifidobacterium subtile str. F395; DSM 20096 NR_029139.1
    #
    } elsif ( $subjectName =~ /^(\d+)\s+(\S+);/ ) {
        $genusSpecies = $1;
                
    # If subject name like:
    # 836673 Escherichia coli str. W CP002185.1 4431305..4432849
    # get string after first space.
    #
    } elsif ( $subjectName =~ /^\S+\s+(.+)/ ) {
        $genusSpecies  = $1;
    
    # If no matches above found, set genus specius name as subject name.
    #
    } else {
        $genusSpecies  = $subjectName;
    }
    $genusSpecies =~ s/^\s+|\s+$//g;
    
    # Remove strain, get just genus species.
    #
    if (!$useFullName && $genusSpecies  =~ /\s/ ) {
        my @words = split /\s+/, $genusSpecies ;
        $genusSpecies  = "$words[0] $words[1]";
    }
    
    return $genusSpecies;
}

#============================================================================#
sub getTaxIdFromName {
    my $self = shift;
    my $name = shift;

    my @res = ();
    my $dbhNames = $self->{dbhNames};
    my $sth = $dbhNames->prepare("SELECT taxid, name from taxtonames where name=?"); 
    $sth->execute($name);
    while (my @rows=$sth->fetchrow_array()) {
        push @res, \@rows;
    }
    return @res;
}

#============================================================================#
sub getTaxIdFromGiNumber {
    my $self = shift;
    my $giNumber = shift;
    my $digitLen = $self->{isTableFormat3digits} ? 3:2;
    my $table = "gi_to_tax_".substr($giNumber, length($giNumber)-$digitLen, length($giNumber));
        my $taxid = 0;
    my $dbhGiTax = $self->{dbhGiTax};
    my $sth = $dbhGiTax->prepare("SELECT taxid from $table where gi=?");
    $sth->execute($giNumber);
    while (my $row=$sth->fetchrow_arrayref()) {
        $taxid = $$row[0];
        last;
    }
    return $taxid;
}

#============================================================================#
sub getNameFromTaxId {
    my $self = shift;
    my $taxid = shift;

    my @res = ();
    my $dbhNames = $self->{dbhNames};
    my $sth = $dbhNames->prepare("SELECT taxid, name from taxtonames where taxid=?"); 
    $sth->execute($taxid);
    while (my @rows=$sth->fetchrow_array()) {
        push @res, \@rows;
    }
    return @res;
}

#============================================================================#
sub whatdatax {
    my $self = shift;
    my $name = shift;
    my $hrefParam = shift || {};

    # $hrefParams:
    # - taxId: lookup by tax id instead of gi number
    
    my @tax_ids = ();
    my %list = ();
    my @outputs = ();

    # locate name if name provided
    if (defined $$hrefParam{taxId} && $$hrefParam{taxId}) {
        push @tax_ids, $name;
    } else {
        $self->_name2tax($name, \@tax_ids);
    }

    # do mapping if taxid found
    if(@tax_ids){
        $self->_taxid2list(\@tax_ids, \%list);
    }

    if(%list){
        my @final_list;
        foreach my $i (sort{$b <=> $a} keys(%list)){
    #        if(   $list{$i}{'rank'} eq 'superkingdom'   #0
    #           || $list{$i}{'rank'} eq 'phylum'         #1
    #           || $list{$i}{'rank'} eq 'class'          #2
    #           || $list{$i}{'rank'} eq 'order'          #3
    #           || $list{$i}{'rank'} eq 'family'         #4
    #           || $list{$i}{'rank'} eq 'genus'          #5
    #           || $list{$i}{'rank'} eq 'species'        #6
    #        ) { 
    #            print "FINALPRINT: $i $list{$i}{'rank'} $list{$i}{'name'};\n" if $DEBUG;
    #            $final_list .= $list{$i}{'name'} . ';';
    #        }
            $final_list[0] = $list{$i}{'name'} if $list{$i}{'rank'} eq 'superkingdom';
            $final_list[1] = $list{$i}{'name'} if $list{$i}{'rank'} eq 'phylum';
            $final_list[2] = $list{$i}{'name'} if $list{$i}{'rank'} eq 'class';
            $final_list[3] = $list{$i}{'name'} if $list{$i}{'rank'} eq 'order';
            $final_list[4] = $list{$i}{'name'} if $list{$i}{'rank'} eq 'family';
            $final_list[5] = $list{$i}{'name'} if $list{$i}{'rank'} eq 'genus';
            $final_list[6] = $list{$i}{'name'} if $list{$i}{'rank'} eq 'species';
     
        }

        #fill in any uninitilized values
        for(my $j=0; $j<=6; $j++){
            $final_list[$j] = '' unless $final_list[$j];
        }

        my $list = join(';', @final_list);
        push @outputs, $list;
    }
    
    return @outputs;
}

#============================================================================#
sub mkCharsFriendly {
    my $self = shift;
    my $string = shift;
    
    $string =~ s/\s+/_/g; # replace whitespace with '_'.
    $string =~ s/\//_/g; # replace '/' with '_'.
    $string =~ s/\(/_/g; # replace '(' with '_'.
    $string =~ s/\)/_/g; # replace '(' with '_'.
    $string =~ s/\'/_/g; # replace single quote with '_'.
    $string =~ s/\"/_/g; # replace double quote with '_'.
    $string =~ s/\_+/_/g; # replace consecutive multiple '_' with single.
    
    return $string;
}

#============================================================================#
## takes name and attempts to find tax id for the name
sub _name2tax{
    my $self = shift;
    my $names_query = shift; 
    my $tax_ids_aref = shift;

    my $dbhNames = $self->{dbhNames};
    my @words = split(/_|\s+/, $names_query);
    while(@words){
        my $term = join(' ', @words);
        $term = lc($term);
        $term =~ s/\'//g; # remove any single quotes from term.
        my $query = $dbhNames->selectall_arrayref("SELECT taxid FROM taxtonames WHERE name_lc = '$term'");
        foreach my $l (@$query){
            my ($return) = @$l;
            if($return){
                push(@$tax_ids_aref, $return);
                return 1; 
            }
        }	
        pop(@words);	       
    }
}

#============================================================================#
sub _taxid2list {
    my $self = shift;
    my $tax_id_aref = shift;
    my $list_href = shift;

    foreach my $taxid (@$tax_id_aref){
        next unless $taxid;
        my ($parent_tax, $rank);
        my $i = 1;
        do{
            #lookup name
            my $name = $self->_tax2name($taxid);
            next unless $name;
            ($parent_tax, $rank) = $self->_findparent($taxid);
            $$list_href{$i}{'rank'} = $rank;
            $$list_href{$i}{'name'} = $name;
            $taxid = $parent_tax;
            $i++
        } while ($parent_tax != 1);
    }
}

#============================================================================#
# given nodes db and taxid, returns parent tax id and rank
sub _findparent {
    my $self = shift;
    my $taxid = shift;

    my $dbhNodes = $self->{dbhNodes};
    my $query = $dbhNodes->selectall_arrayref("SELECT parenttaxid, rank FROM taxnodes WHERE taxid = $taxid");
    foreach my $l (@$query){
        my ($parenttax, $rank) = @$l;
        return ($parenttax, $rank);
    }
}

#============================================================================#
# given names db and taxid returns name
sub _tax2name{
   my $self = shift;
   my $taxid = shift;
   
   my $dbhNames=  $self->{dbhNames};
   my $query = $dbhNames->selectall_arrayref("SELECT * FROM taxtonames WHERE taxid = $taxid");
   foreach my $l (@$query){
       my ($tax, $name, $lcname) = @$l;
       #print "QUERY tax: $tax name: $name lcname: $lcname\n";
       return $name;
    }
}

#============================================================================#
sub DESTROY {
    my $self = shift;
    my $dbhNames = $self->{dbhNames};
    my $dbhNodes = $self->{dbhNodes};
    $dbhNames->disconnect();
    $dbhNodes->disconnect();
}

#============================================================================#

1;
