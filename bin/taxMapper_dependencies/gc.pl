#!/usr/bin/env perl

use strict;
use Getopt::Long;

my $usage = q(

PROGRAM: gc.pl (G+C Statistics)

DESCRIPTION: To determine the G+C content of input sequences

SYNOPSIS (1): cat contigs.fasta | gc.pl
SYNOPSIS (2): cat *fasta | gc.pl > allseqs.gc_stats

INPUT: one or more DNA sequences in FASTA format, read from standard input

OUTPUT:
- prints stats on G+C content for each contig and genome-wide
- prints to standard out; you may redirect to a file with '>' operator

OPTIONS:
-v : verbose mode prints G+C content for each contig

Please address your comments to ESKirton@LBL.gov.

);

my ($help, $verbose);
GetOptions(
	'h' => \$help,
	'v' => \$verbose
);

if ($help)
{
	print $usage;
	exit;
}

my $contig_id;
my $GC     = 0;
my $AT     = 0;
my $tot_GC = 0;
my $tot_AT = 0;
my $pct_GC;
my $tot;

print "CONTIG\tG+C\tA+T\ttot\t%G+C\n" if $verbose;

while (<STDIN>)
{
	chomp;
	next unless $_;
	if (/^>(\S+)/)
	{
		my $temp_contig_id = $1;

		###########################
		# PROCESS PREVIOUS CONTIG #
		###########################
		if (defined($contig_id))
		{
			$tot    = $GC + $AT;
			#$pct_GC = int($GC / $tot * 1000 + .5) / 10;    # ROUND TO 1 DECIMAL PLACE
			if ($tot) {
				$pct_GC = 100 * $GC / $tot;
			}
			else {
				$pct_GC = 0.00;
			}
			printf "%s\t%d\t%d\t%d\t%7.3f\n", $contig_id, $GC, $AT, $tot, $pct_GC if $verbose;
			#print "$contig_id\t$GC\t$AT\t$tot\t$pct_GC\n" if $verbose;
			$tot_GC += $GC;
			$tot_AT += $AT;
		}

		###########################################
		# PREPARE DATA CONTAINERS FOR THIS CONTIG #
		###########################################
		$contig_id = $temp_contig_id;
		$GC = $AT = 0;

	} else
	{
		$GC += s/g//gi;
		$GC += s/c//gi;
		$AT += s/a//gi;    # just to be sure (e.g. in case there are spaces, ambiguous nucleotide characters, etc.)
		$AT += s/t//gi;    # just to be sure
	}
}

#######################
# PROCESS LAST CONTIG #
#######################
if (defined($contig_id))
{
	$tot    = $GC + $AT;
	if ($tot) {
		$pct_GC = 100 * $GC / $tot;
	}
	else {
		$pct_GC = 0.00;
	}
	printf "%s\t%d\t%d\t%d\t%7.3f\n", $contig_id, $GC, $AT, $tot, $pct_GC if $verbose;
	$tot_GC += $GC;
	$tot_AT += $AT;
}

################################
# CALCULATE GENOME-WIDE TOTALS #
################################

$tot = $tot_GC + $tot_AT;
die("ERROR: No sequences provided\n") unless $tot;
my $pct_GC_tot = 0.00;
if ($tot) {
	$pct_GC_tot = int($tot_GC / $tot * 1000 + .5) / 10;
}
#print "\n# AVERAGES FOR ALL CONTIGS:\n", "# TOTAL G+C = $tot_GC bp\n", "# TOTAL A+T = $tot_AT bp\n", "# TOTAL LEN = $tot bp\n", "#      %G+C = $pct_GC_tot%\n";
