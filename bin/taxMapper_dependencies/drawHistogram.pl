#!/usr/bin/env perl

=head1 NAME

drawHistogram.pl

=head1 SYNOPSIS

drawHistogram.pl [options] <file>

  Options:
  -c <number>        Column containing observations (optional; default=1)
  -cn <number>       Column containing number of observation (optional)
  -b <number>        Bin size (optional; default=1)
  -d <character>     Column delimiter (optional; default=one or more space or tab)
  -s <number>        Starting bin (optional; default=0)
  -min <number>      Minimum observation size cutoff (optional)
  -max <number>      Maximum observation size cutoff (optional)
  -csv               Display in csv format (optional)
  -nh                Do not display header info (optional)
  -se                Show empty bins (optional)
  -h                 Detailed help message (optional)

  Takes a stream of column separated values and generates a histogram of the
  dataset.

  Usage: drawHistogram.pl -c 1 inputFile > outputFile or
         cat inputFile | drawHistogram.pl -c 1 > outputFile

=head1 DESCRIPTION

Draws histogram from dataset present in a file.  The file should contain a list
of values within a single column.  Multiple columns may exist in the file.
There are two ways for specifying the dataset within the specified file to
display the histogram.

Method 1:
The file contains the observations within a single column.

drawHistogram.pl -c <column number> <file>

or if the first column in the file contains the observations:

drawHistogram.pl <file>

Method 2:
The file contains the bin values in one column, the totals of the observations
within that bin in another column.

drawHistogram -c <column_w_observation_values> -cn <column_w_observation_totals> <file>

If the 1st column contains the bin number, -c option may be omitted.

The default separator between columns is one or more white spaces (tabs, space).
If the columns are separated by a different delimiter, specify the delimiter
using the -d option.

A line starting with '#' in the file will be ignored.

If the bin size is set to zero (-b 0), then no binning is done and the
ending bin position of the last bin is the current starting bin - 1.

=head1 COPYRIGHT

DOE Joint Genome Institute Microbial Genomics

Copyright (C) 2007 The Regents of the University of California
All rights reserved.

NOTICE: The Government is granted for itself and others acting on its
behalf a paid-up, nonexclusive irrevocable worldwide license in this
data to reproduce, prepare derivative works, and perform publicly and
display publicly. Beginning five (5) years after permission to assert
copyright is granted, subject to two possible five year renewals, the
Government is granted for itself and others acting on its behalf a
paid-up, non-exclusive, irrevocable worldwide license in this data to
reproduce, prepare derivative works, distribute copies to the public,
perform publicly and display publicly, and to permit others to do so.
NEITHER THE UNITED STATES NOR THE UNITED STATES DEPARTMENT OF ENERGY,
NOR ANY OF THEIR EMPLOYEES, MAKES ANY WARRANTY, EXPRESS OR IMPLIED,
OR ASSUMES ANY LEGAL LIABILITY OR RESPONSIBILITY FOR THE ACCURACY,
COMPLETENESS, OR USEFULNESS OF ANY INFORMATION, APPARATUS, PRODUCT,
OR PROCESS DISCLOSED, OR REPRESENTS THAT ITS USE WOULD NOT INFRINGE
PRIVATELY OWNED RIGHTS.

=head1 VERSION

$Revision: 1.4 $

$Date: 2007/04/12 19:39:17 $

=head1 HISTORY

=over

=item *

Stephan Trong 03/15/2007 Creation

=back

=cut

use strict;
use warnings;
use Getopt::Long;
use Pod::Usage;
use File::Basename;
use FindBin;
use lib "$FindBin::RealBin";
use QDExtension::Histogram;
use vars qw( $optHelp $optDelimiter $optInitialMinBin $optColumnOfObservations
$optColumnOfObservationTotals $optBinSize $optDisplayCondenseReport
$optMinObservationSize $optMaxObservationSize $optNoHeader $optShowEmptyBins);

#============================================================================#
# INPUT VALIDATION
#============================================================================#
if( !GetOptions(
        "d=n"=>\$optDelimiter,
        "c=n"=>\$optColumnOfObservations,
        "cn=n"=>\$optColumnOfObservationTotals,
        "i=n"=>\$optInitialMinBin,
        "b=f"=>\$optBinSize,
        "csv"=>\$optDisplayCondenseReport,
        "nh"=>\$optNoHeader,
        "min=n"=>\$optMinObservationSize,
        "max=n"=>\$optMaxObservationSize,
        "se"=>\$optShowEmptyBins,
        "h"=>\$optHelp,
    )
) {
    pod2usage();
    exit 1;
}

if ( defined $optHelp ) {
    pod2usage(-verbose=>2);
    exit 1;
}
#pod2usage and exit 1 if @ARGV != 1;

#my $inputFile = $ARGV[0];
my $binSize = defined $optBinSize ? $optBinSize:1;

if ( $binSize < 0 ) {
    print STDERR "Binsize must be zero or greater.\n";
    exit 1;
}


#============================================================================#
# INITIALIZE VARIABLES
#============================================================================#
my $delimiter = defined $optDelimiter ? $optDelimiter : '\s+';
my $initialMinBin = defined $optInitialMinBin ? $optInitialMinBin : 0;

$optColumnOfObservations = 1 if !defined $optColumnOfObservations;

#============================================================================#
# MAIN
#============================================================================#

# Get observations from file.
#

if ( defined $optColumnOfObservationTotals ) {
    my %observations = getObservationsByColumnAndTotal($optColumnOfObservations,
        $optColumnOfObservationTotals, $delimiter);
    
    if ( !%observations ) {
        print STDERR "Cannot find any observations.\n";
        exit 1;
    }
    
    # Draw histogram.
    #
    drawHistogram($binSize, $initialMinBin, \%observations);
    
} else {
    my @observations = getObservationsByColumn($optColumnOfObservations,
        $delimiter);
    
    if ( !@observations ) {
        print STDERR "Cannot find any observations.\n";
        exit 1;
    }
    
    # Draw histogram.
    #
    drawHistogram($binSize, $initialMinBin, \@observations);
}

exit 0;

#============================================================================#
# SUBROUTINES
#============================================================================#
sub getObservationsByColumnAndTotal {
    
    my $columnOfObservations = shift;
    my $columnOfTotals = shift;
    my $delimiter = shift;
    
    my %observations = ();
    my $largestColumn = $columnOfObservations > $columnOfTotals ?
        $columnOfObservations : $columnOfTotals;
        
    while (<>) {
        next if !length $_ || /^#/;
        my @cols = split(/$delimiter/);
        next if @cols < $largestColumn;
        
        my $observation = $cols[$columnOfObservations-1];
        my $total = $cols[$columnOfTotals-1];
        if ( $observation =~ /^([-+])*\d+(\.\d+)*/ ) {
            $total = 0 if $total !~ /^([-+])*\d+(\.\d+)*/;
            $observations{$observation} += $total;
        }
    }
    
    return %observations;
    
}

#============================================================================#
sub getObservationsByColumn {
    
    my $column = shift;
    my $delimiter = shift;
    
    my @observations = ();
    
    while (<>) {
        next if !length $_ || /^#/;
        my @cols = split(/$delimiter/);
        next if @cols < $column;
        my $observation = $cols[$column-1];
        push @observations, $observation if $observation =~ /^([-+])*\d+(\.\d+)*/;
    }
    
    return @observations;
    
}

#============================================================================#
sub drawHistogram {
    
    my $binSize = shift;
    my $initialMinBin = shift;
    my $refHashOrArrayOfObservations = shift;
    
    # Create object to create histogram.
    #
    my $histObject = JGI::QDExtension::Histogram->new();
    
    $histObject->showEmptyBins(1) if defined $optShowEmptyBins;

    $histObject->setMinObservation($optMinObservationSize)
        if defined $optMinObservationSize;
        
    $histObject->setMaxObservation($optMaxObservationSize)
        if defined $optMaxObservationSize;
        
    # Create histogramof depth of coverage.
    #
    $histObject->computeHistogram(
        $refHashOrArrayOfObservations,
        $binSize,
        $initialMinBin
    );
    
    my $pictureFormat = defined $optDisplayCondenseReport ? 'csv':'detailed';

    my %params = ( format=>$pictureFormat );
    $params{noHeader} = $optNoHeader if defined $optNoHeader;
    
    print $histObject->getHistogramPicture(%params);
}

#============================================================================#
