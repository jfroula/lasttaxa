#!/usr/bin/env perl
#
=head1 NAME

parsefasta.pl - reports the sequence length for each fasta entry.

=head1 SYNOPSIS

  parsefasta.pl [options] <file_containing_headers> <fasta_file>
  
  Options:
  -r    Remove entries found in <file_containing_headers> (default=get entries).
  -h    Help message (optional)

=head1 DESCRIPTION

$Revision: 1.10 $

$Date: 2009-08-26 17:18:14 $

=head1 AUTHOR(S)

Stephan Trong

=head1 HISTORY

=over

=item *

S.Trong 2008/11/06 creation

=back

=cut

use strict;
use warnings;
use Pod::Usage;
use Getopt::Long;
use FindBin;
use lib "$FindBin::RealBin/../lib";
use JGI::Parsers::FastaParse;
use vars qw( $optHelp $optReverse );

#============================================================================#
# INPUT VALIDATION
#============================================================================#
if( !GetOptions(
        "r"=>\$optReverse,
        "h"=>\$optHelp,
    )
) {
    usage();
}

pod2usage(2) if $optHelp;
pod2usage and exit 1 if @ARGV != 2;

#============================================================================#
# INITIALIZE VARIABLES
#============================================================================#

my ($inputHeaderFile, $inputFastaFile) = @ARGV;

#============================================================================#
# VALIDATE INPUTS
#============================================================================#

if ( !-s $inputFastaFile ) {
    print STDERR "The file $inputFastaFile you specified does not exist or is zero size.\n";
    exit 1;
}

if ( !-s $inputHeaderFile)  {
    print STDERR "The file $inputHeaderFile you specified does not exist or is zero size.\n";
    exit 1;
}
#============================================================================#
# MAIN
#============================================================================#

my $minLength;
my $maxLength;
my $minLengthEntry;
my $maxLengthEntry;
my $sum = 0;
my %headers = getInputFastaHeaders($inputHeaderFile);
my $objFastaParser = new JGI::Parsers::FastaParse($inputFastaFile);

while ($objFastaParser->MoreEntries) {
    $objFastaParser->ReadNextEntry( -rawFormat=>1 );
    my $fastaName = $objFastaParser->Name;
    my $fastaComment = $objFastaParser->Comment;
    my $seq = $objFastaParser->Seq;
    
    if ( $optReverse && !$headers{$fastaName} ) {
        print ">$fastaName";
        print " $fastaComment" if length $fastaComment;
        print "\n$seq";
    } elsif ( !$optReverse && $headers{$fastaName} ) {
        print ">$fastaName";
        print " $fastaComment" if length $fastaComment;
        print "\n$seq";
    }
}

#============================================================================#
# SUBROUTINES
#============================================================================#
sub getInputFastaHeaders {
    my $file = shift;
    
    my %headers = ();
    open IN, $file or die "Failed to open file $file: $!\n";
    while (<IN>) {
	$_ =~ s/^\s+|\s+$//g;
	$headers{$_} = 1 if length $_;
    }
    close IN;
    
    return %headers;
}

#============================================================================#
sub usage {
    my $verbose = shift || 1;
    pod2usage(-verbose=>$verbose);
    exit 1;
}

#============================================================================#
