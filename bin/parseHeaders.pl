#!/usr/bin/env perl
# this script will take as input a fasta file of genomes like NR database
# and create a table of protein accession numbers and either a 1 or 0.
# A '0' signifies that one of the key words were found in the protein
# description, i.e. unknown protein, and a '1' signifies that there was 
# not a keyword, i.e. the protein name is known.
#
# Key words:
# unknown|hypothetical|putative|uncharacterized

use strict;
use warnings;

#============================================================================#
# INPUT VALIDATION
#============================================================================#
my $fasta = $ARGV[0];
my $accessions = "accNums.tab";
die "Usage: $0 <fasta database>\n" unless $fasta;
die "Missing or empty fasta file: $fasta\n" unless -s $fasta;
die "Missing or empty accessions file: $accessions\n" unless -s $accessions;

#============================================================================#
# MAIN
#============================================================================

#my %lookup = ();
#open ACC,"$accessions" || die "can't open $accessions\n";
#while (<ACC>) {
#    chomp;
#    $lookup{$_}='';
#}
#my @accessions = <ACC>;
#close ACC;

my @grepResults = grep{//} @accessions;
my ($id,$info);
open FASTA,"$fasta" || die "can't open $fasta\n";
while (<FASTA>) {
    chomp;
    if (/^(>.*)/){
        my $wholeHeader = $1;
        while ($wholeHeader =~ /([^\cA]+)/g){
            my $line = $1;
            next if $line =~ /^\s*$/;
            $line =~ s/>//;
            my $accession = returnAccession($line);
#            my @answer = grep{/unknown|hypothetical|putative|uncharacterized/} $line;
#            if (@answer){
            if (exists $lookup{$accession}){
                print "$accession\t$line\n";
#            }else{
#                print "$accession\tknown\n";
            }
        }    
    }    
}    

close FASTA;

#============================================================================#
# SUBROUTINES
#============================================================================#
sub returnAccession {
   my ($line) = @_; 
   if ($line =~ /^gi\|\d+\|.*?\|(.*?)\|/){
        return $1; 
   }elsif($line =~ /^\s*(\S+)/){
        return $1; 
   }else{
        warn "can't parse line: $line\n";
   }
}

__END__
#
# Identify percent unannotated proteins
#
hits=$(wc -l $ginums | cut -f1 -d' ')
tot=$(grep -c "^>" prots.faa)
unknown=$(awk -F"\t" '$2==0' $query | wc -l)
unan=$(((tot - hits) + unknown))
per=$(echo "scale=4;$unan/$tot"|bc)
#echo "$tot $unknown $unan $per"

echo "$BINID $per $tot " > $BINID.unannotated
