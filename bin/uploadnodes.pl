#!/usr/bin/env perl

use strict;
use warnings;
use DBI;

    my $nodes_file = "nodes.dmp";  
    unless(-f $nodes_file){
        print "cannot find $nodes_file\n";
        exit;
    }

    my $db = DBI->connect("dbi:SQLite:nodes.db", "", "", {RaiseError => 1, AutoCommit => 0});
    $db -> do("drop table if exists taxnodes");
    $db -> do("create table taxnodes(taxid integer primary_key, parenttaxid integar, rank)");
    
    open(my $nodes_fh, $nodes_file) or die $!;
    my $i = 0;
    while(my $line = <$nodes_fh>){
        $line =~ s/\t\|\n//;
        $i++;
        my @fields = split(/\t\|\t/, $line);
        my $tax_id = $fields[0];
        my $parent_tax_id = $fields[1];
        my $rank = $fields[2];
        #print "TAXID: $tax_id PTAXID: $parent_tax_id RANK: $rank \n";
        $db->do("INSERT INTO taxnodes values(\"$tax_id\", \"$parent_tax_id\", \"$rank\")");   
        print "$i uploaded\n" if $i % 100000 == 1;
    }

    $db ->do("create index taxid_index on taxnodes(taxid)");
    $db ->do("create index parenttaxid_index on taxnodes(parenttaxid)");
    $db->commit() or die $db->errstr;
 
    my $query = $db->selectall_arrayref("SELECT * FROM taxnodes");
    my $j = 0;
#    foreach my $l (@$query){
#        my ($tax, $ptax, $rank) = @$l;
#        $j++;
#        print "QUERY row: $j  tax: $tax parenttax: $ptax rank: $rank\n";
#    }
  
    $db->disconnect();
