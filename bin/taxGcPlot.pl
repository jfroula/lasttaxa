#!/usr/bin/env perl

=head1 NAME

taxGcPlot.pl

=head1 SYNOPSIS

  taxGcPlot.pl [options] <blast.parsed file> <queryFastaFile> <imageName>

  Options:

  -tax_dump              directory of NCBI's taxonomic names.dmp and nodes.dmp tables (required).
  -t <number>            Specify the taxonomic classifications to plot (required)
                         Valid input values:
                           1 - Kingdom
                           2 - Phylum
                           3 - Class
                           4 - Order
                           5 - Family
                           6 - Genus
                           7 - Species
                           8 - Sub-species
                           9 - All
                         Specify multiple -t <number> flags to produce multiple plots
                         for different classifications.
  -ft <fileType>         Specify a file type to create (png, svg, eps). Optional;
                         default=png. Use multiple -ft options to specify more
                         than one file type.
  -b <number>            Specify GC bin size (optional; default=1)
  -ylabel <string>       Specify y-axis label (optional; default="% Hits")
  -title <string>        Specify title of plot (optional)
  -replot <dataDir>     Use this to replot only (skips creation of fasta files,
                         assuming these files already exist in the <dataDir> dir;
                         input is dir containing pre-generated files).
  -pctCutoff <number>    Don't display hits if percent is less than this (optional
                         default=0)
  -skipTotal             Don't plot GC histogram of the original fasta file.
  -od <directory>        Specify output dir (optional; default is current dir).
  -dataDir <directory>   Specify directory to store data files (optional;
                         default=<working_dir>/data, do not specify
                         if using -replot option).
  -h                     Detailed message (optional).

  Takes a blast parsed file generated from running blast+ and generates
  GC plots for each seq fragment that has a hit, colored by organism name.
  
  The blast.parsed file is a tab delimited blast file containing the following
  columns:
  - qseqid
  - sseqid 
  - bitscore 
  - evalue 
  - length 
  - pident 
  - qstart 
  - qend 
  - qlen 
  - qframe 
  - sstart 
  - send 
  - slen 
  - sframe

The blast output can be formatted when running blast+ with the options
-outfmt '6 qseqid sseqid bitscore evalue length pident qstart qend qlen qframe
sstart send slen sframe'

Note that the sseqid column should contain the scientific name, otherwise the
plot will not show the organism names properly.

Example, column 2 from blast output:
gi|57158257|dbj|AP006725.1|

needs to be changed to :
gi|57158257i|dbj|AP006725.1| Klebsiella pneumoniae subsp. pneumoniae NTUH-K2044
DNA, complete genome

This can be done using the blastdbcmd tool to get the scientific name from the
gi number.

=head1 HISTORY

=over

=item *

S.Trong 2011/03/09 creation

=back

=cut  

use strict;
use warnings;
use Getopt::Long;
use Pod::Usage;
use File::Basename;
use File::Path;
use File::Copy;
use File::Temp qw(tempfile tempdir);
use Tie::IxHash;
use Cwd;
use Cwd 'abs_path';
use vars qw( $optTaxDump $optHelp $optOutputDir $optBinSize $optYlabel @optTaxClasses
    $optReplotOnly $optSkipGcTotal $optDataDir $optTitle $optPctCutoff
    @optOutputFormats $optMakeTophit);

#============================================================================#
# INPUT PARAMETERS
#============================================================================#
my $programExecution = abs_path(dirname($0))."/".basename($0)." @ARGV";
if( !GetOptions(
            "tax_dump=s"=>\$optTaxDump,
            "od=s"=>\$optOutputDir,
            "ft=s@"=>\@optOutputFormats,
            "ylabel=s"=>\$optYlabel,
            "b=f"=>\$optBinSize,
            "t=s@"=>\@optTaxClasses,
            "replot=s"=>\$optReplotOnly,
            "skipTotal"=>\$optSkipGcTotal,
            "dataDir=s"=>\$optDataDir,
            "pctCutoff=f"=>\$optPctCutoff,
            "tophit"=>\$optMakeTophit,
            "title=s"=>\$optTitle,
            "h"=>\$optHelp,
        )
    ) {
    printhelp(1);
}

printhelp(1) if @ARGV != 3;
die "the tax_dump option is required. You need to include a path to nodes.dmp and names.dmp.\n"  
  if not $optTaxDump;

#============================================================================#
# INITIALIZE VARIABLES
#============================================================================#
use constant SUCCESS => 0;
use constant FAILURE => 1;

my $externalToolsPath = abs_path(dirname($0))."/taxMapper_dependencies";

my $TAXMAPPER_EXE = "$externalToolsPath/taxMapper.pl";
my $GNUPLOT_EXE = 'gnuplot';

my $GC_EXE = "$externalToolsPath/gc.pl";
my $BLAST2TOPHITS_EXE = "$externalToolsPath/blast2tophits.sh";
my $DRAW_HISTOGRAM_EXE = "$externalToolsPath/drawHistogram.pl";
my $FASTAPARSER_EXE = "$externalToolsPath/parsefasta.pl";

my %taxClasses = (
    1=>"Kingdom",
    2=>"Phylum",
    3=>"Class",
    4=>"Order",
    5=>"Family",
    6=>"Genus",
    7=>"Species",
);

my %inputClasses = ();

my $inputBlastFile = shift @ARGV;
my $inputFastaFile = shift @ARGV;
my $imageLabel = shift @ARGV;
my $outputDir = defined $optOutputDir ? $optOutputDir : getcwd;
   $outputDir =~ s/\/+$//; # remove trailing '/'
my $dataDir = defined $optDataDir ? $optDataDir : "$outputDir/data";
   $dataDir = $optReplotOnly if defined $optReplotOnly;
   $dataDir =~ s/\/+$//; # remove trailing '/'

$imageLabel =~ s/\.png$//;
$inputBlastFile = abs_path($inputBlastFile);
$inputFastaFile = abs_path($inputFastaFile);

$optBinSize = 1 if !defined $optBinSize;
$optYlabel = "% Hits" if !defined $optYlabel;
$optSkipGcTotal = 0 if !defined $optSkipGcTotal;
$optPctCutoff = 0 if !defined $optPctCutoff;

@optOutputFormats = ("png") if !@optOutputFormats;

#============================================================================#
# Checks
#============================================================================#

if ( !-s $inputBlastFile ) {
    print "The input file $inputBlastFile does not exist or is zero size.\n";
    exit 1;
}

if ( !-s $inputFastaFile ) {
    print "The input file $inputFastaFile does not exist or is zero size.\n";
    exit 1;
}

if ( !@optTaxClasses ) {
    print "The -t <number> option is required.\n";
    exit 1;
}
if ( !-e "$optTaxDump/nodes.dmp" || !-e "$optTaxDump/names.dmp") {
    print "Failed to find the taxonomic dump files names.dmp and/or nodes.dmp in $optTaxDump.\n";
    exit 1;
}

if ( @optOutputFormats ) {
    foreach my $format (@optOutputFormats) {
        if ( $format !~ /^svg|eps|png$/ ) {
            print "The -ft $format option you specified is not valid. ".
                "Valid arguments are png, svg or ps.\n";
            exit 1;
        }
    }
}

# Check classification input and create hash of classes.
#
foreach my $value (@optTaxClasses) {
    if ( $value !~ /^\d+$/ || $value < 0 || $value > 9 ) {
        print "The -t $value option is invalid. Specify 1-9.\n";
        exit 1;
    }
    if ( $value == 9 ) {
        foreach my $tax (keys %taxClasses) {
            $inputClasses{$taxClasses{$tax}} = 1;
        }
        last;
    } else {
        $inputClasses{$taxClasses{$value}} = 1;
    }
}

#============================================================================#
# MAIN
#============================================================================#
# Create output dirs if not exist.
#
mkpath( $outputDir ) if ($optOutputDir && !-e $outputDir);
mkpath( $dataDir ) if !-e $dataDir;

my $fastaBase = basename($inputFastaFile);
my $statsFile = "$dataDir/stats.txt";

# Create tax mapping files.
#
my $tophitFile = '';
if ( runTaxMapper($inputBlastFile, $inputFastaFile, $dataDir) != SUCCESS ) {
    exit 1;
}

if ( !-s $statsFile ) {
    print STDERR "Cannot find file $statsFile.\n";
    exit 1;
}

# Parse top hits by tax levels from stats file.
#
my ($hrefTopHitCats, $hrefTopHitTotalsByCat) = getTopHitCategories($statsFile);

# Create tax plots.
#
foreach my $taxClass ( keys %inputClasses ) {
    my $outputImageFile = "$outputDir/${imageLabel}_$taxClass";
    my $title = defined $optTitle ? "$taxClass level $optTitle" :
        "$taxClass level GC for $fastaBase";
    
    # Create GC plot.
    #
    if ( createGcPlot(\@optOutputFormats, $hrefTopHitCats, $hrefTopHitTotalsByCat,
        $dataDir, $inputBlastFile, $outputImageFile, $inputFastaFile, $taxClass,
        $optBinSize, $optYlabel, $optSkipGcTotal, $title) != SUCCESS ) {
        print STDERR "Failed to create image file for $taxClass. skipping...\n";
        next;
    }
}

exit 0;

#============================================================================#
# SUBROUTINES
#============================================================================#
sub convertBlastToTophits {
    my $parsedFile = shift;
    my $tophitFile = shift;
    
    # Run script to convert *.parsed file to *.tophit file.
    #
    my $cmdParseToTophits = "$BLAST2TOPHITS_EXE $parsedFile";
    print "$cmdParseToTophits\n\n";
    
    if ( system ( $cmdParseToTophits ) != 0 ) {
        print STDERR "$cmdParseToTophits failed to execute: $?\n";
        return FAILURE;
    }
    
    if ( !-s $tophitFile ) {
        print STDERR "Failed to create file $tophitFile.\n";
        return FAILURE;
    }
    
    # If from blastp output file and mapped to fasta files created
    # by prodigal, chop off ID=... string appended to the read name.
    #
    my $cmd = "perl -p -i -e 's\/^(\\S+)(ID=\\S+)\\s+(.+)\/\$1 \$3\/' $tophitFile";
    system($cmd);
    
    return SUCCESS;
}

#============================================================================#
sub runTaxMapper {
    my $tophitFile = shift;
    my $fastaFile = shift;
    my $dataDir = shift;
    
    my $cmd = $TAXMAPPER_EXE. " -dbpath $optTaxDump -od $dataDir $tophitFile $inputFastaFile";
    print "$cmd\n\n";
    
    if ( system ( $cmd ) != 0 ) {
        print STDERR "$cmd failed to execute: $?\n";
        return FAILURE;
    }
    
    return SUCCESS;
}
    
#============================================================================#
sub getTopHitCategories {
    my $statsFile = shift;
    
    tie my %topHitCats, "Tie::IxHash";
    tie my %topHitTotalsByCat, "Tie::IxHash";
    
    open INFILE, $statsFile or die "ERROR: failed to open file $statsFile: $!\n";
    while (<INFILE>) {
        chomp;
        # parse string like "Species Trichoplax_adhaerens 1"
        # $1 = tax level (e.g., Kingdom)
        # $2 = name of hit (e.g.., Eukaryota)
        # $3 = total
        if ( /^(\S+)\s+(\S+)\s+(\d+)/ ) {
            $topHitCats{$1}{$2} = $3;
            $topHitTotalsByCat{$1} += $3;
        }
    }
    
    return \%topHitCats, \%topHitTotalsByCat;
}

#============================================================================#
sub createGcPlot {
    my $arefOutputFormats = shift;
    my $hrefTopHitCats = shift;
    my $hrefTopHitTotalsByCat = shift;
    my $outputDir = shift;
    my $blastFile = shift;
    my $imageFile = shift;
    my $fastaFile = shift;
    my $taxClass = shift;
    my $binSize = shift;
    my $yLabel = shift;
    my $skipGcTotal = shift;
    my $title = shift;
    
    my %gcFiles = ();
    my %gcCats = ();
    my %gcDatas = ();
    my %fileFormats = ();
    @fileFormats{@$arefOutputFormats} = (1) x @$arefOutputFormats;
    
    # Create GC data for subsample input fasta file.
    #
    my $gcFile= "$outputDir/".basename($fastaFile).".b$binSize.GC";
    my $histFile = "$gcFile.hist";
    
    if ( createGcDataFile($fastaFile, $gcFile, $histFile, $binSize) !=
        SUCCESS ) {
        return FAILURE;
    }
    
    if ( !$skipGcTotal) {
        $gcFiles{"Total"} = $histFile;
        $gcCats{$histFile} = "Total";
    }
    my $subsampleSize = getSubsampleSize($histFile);
    my %gcTotalsByBin = getGcTotals($histFile);
    $gcDatas{"Total"} = \%gcTotalsByBin;

    # Create GC data for each hit.
    #
    opendir ( DIR , $outputDir ) or die "Can't open $outputDir\n";
    while( my $fastaFile = readdir(DIR) ){
	next unless $fastaFile =~ /^$taxClass\.(.*).fasta$/;
        my $category = $1;
        my $numHits = defined $$hrefTopHitCats{$taxClass}{$category} ? $$hrefTopHitCats{$taxClass}{$category}:0;
        my $pctHits = defined $$hrefTopHitCats{$taxClass} ? $numHits/$$hrefTopHitTotalsByCat{$taxClass}*100:0;
        
        # Skip if pct hits for this organism is less than max pct cutoff.
        #
        if ($pctHits < $optPctCutoff) {
            next;
        }
        
        my $gcFile = $skipGcTotal ? "$outputDir/$fastaFile.b$binSize.noTotal.GC" :
            "$outputDir/$fastaFile.b$binSize.GC";
        my $histFile = "$gcFile.hist";
        if ( createGcDataFile("$outputDir/$fastaFile", $gcFile, $histFile,
            $binSize) == SUCCESS ) {
            my %gcTotalsByBin = getGcTotals($histFile);
            $gcFiles{$category} = $histFile;
            $gcCats{$histFile} = $category;
            $gcDatas{$category} = \%gcTotalsByBin;
        }
    }	
    close(DIR);
    
    # Sort files by histogram area under the curve. Will use this to
    # overlay the histogram plots so that the ones w/ the least amount
    # of area are displayed in front.
    #
    my @sortedFiles = reverse sortCategoriesByArea(values %gcFiles);
    
    # Get Avg GC of the largest GC curve.
    #
    my $avgGC = getAvgGC( $sortedFiles[0] );
    
    # If avg GC is > 50, then place the legend to the left of the plot.
    # Otherwise, place it to the right.
    #
    my $keyPos = $avgGC > 50 ? "left" : "right";
    
    # Create datafile containing percentage of reads for each GC bin.
    # Use this for plotting.
    #
    my $plotDataFile = "$outputDir/plotDataPct.$taxClass";
       $plotDataFile .= "_nototal" if $skipGcTotal;
       $plotDataFile .= ".csv";
    if ( createGcPlotFile($plotDataFile, \@sortedFiles, \%gcCats, \%gcDatas,
        $subsampleSize) != SUCCESS ) {
        return FAILURE;
    }
    
    my @colors = (
        "#FF0000", # red 
        "#FFFF00", # yellow
        "#008000", # green
        "#FFA500", # orange,
        "#A52A2A", # brown
        "#1E90FF", # dodgerblue
        "#DAA520", # goldenrod
        "#9FAFDF", # navyblue
        "#FF6347", # tomato
        "#8FBC8F", # darkseagreen
        "#DDA0DD", # plum
        "#FFD700", # gold
        "#9932CC", # darkorchid
        "#BC8F8F", # rosybrown
        "#483D8B", # darkslateblue
        "#CD5C5C", # indianred,
        "#00FFFF", # cyan,
    );
    
    # Create png file.
    #
    if ( defined $fileFormats{png} ) {
        my $term = "set terminal png truecolor";
        if ( makeImageFile($term, "$imageFile.png", $title, $yLabel, $keyPos, \@colors,
            \@sortedFiles, \%gcCats, $plotDataFile) != SUCCESS ) {
            return FAILURE;
        }
    }
    
    # Create svg file.
    #
    if ( defined $fileFormats{svg} ) {
        my $term = "set terminal svg truecolor";
        if ( makeImageFile($term, "$imageFile.svg", $title, $yLabel, $keyPos, \@colors,
            \@sortedFiles, \%gcCats, $plotDataFile) != SUCCESS ) {
            return FAILURE;
        }
    }
        
    # Create postscript eps file.
    #
    if ( defined $fileFormats{eps} ) {
        my $term = "set terminal postscript eps truecolor";
        if ( makeImageFile($term, "$imageFile.eps", $title, $yLabel, $keyPos, \@colors,
            \@sortedFiles, \%gcCats, $plotDataFile) != SUCCESS ) {
            return FAILURE;
        }
    }
    
    # Create latex file.
    #
    if ( defined $fileFormats{tex} ) {
        my $term = "set terminal latex";
        if ( makeImageFile($term, "$imageFile.tex", $title, $yLabel, $keyPos, \@colors,
            \@sortedFiles, \%gcCats, $plotDataFile) != SUCCESS ) {
            return FAILURE;
        }
    }
    
    return SUCCESS;
}

#============================================================================#
sub makeImageFile {
    my $terminal = shift;
    my $imageFile = shift;
    my $title = shift;
    my $yLabel = shift;
    my $keyPos = shift; # left|right
    my $arefColors = shift;
    my $arefFiles = shift;
    my $hrefCategories = shift;
    my $plotDataFile = shift;
    
    my @colors = @$arefColors;
    
    # Add newlines to title if length is > $length.
    #
    my $length = 75;
    my $delimiter = "\\n";
    $title =~ s/(.{$length})/$1$delimiter/g;

    # Create gnuplot commands.
    #
    my $gnuCmds = <<EOM;
$terminal;
set output "$imageFile";
set grid;
set style fill transparent solid 0.75 border -1;
set boxwidth 1;
set xlabel "Percent GC";
set xtics 10;
set xrange [0:100];
set ylabel "$yLabel";
set key $keyPos top box;
set title "$title";
set style line 1 lt rgb "gray";
set style line 2 lt rgb "#00CED1";
set style line 3 lt rgb "blue";
EOM

    my $lineNum = 4;
   foreach my $color (@colors) {
        $gnuCmds .= "set style line $lineNum lt rgb \"$color\";\n";
        $lineNum++;
    }
    $gnuCmds .= "plot ";
    
    my $col = 2;
    $lineNum = 4;
    foreach my $file (@$arefFiles) {
        my $category = $$hrefCategories{$file};
        $gnuCmds .= "'$plotDataFile' u 1:$col title \"$category\" with boxes";
        $col++;
        $gnuCmds .= " ls 1," and next if ( $category eq "Total");
        $gnuCmds .= " ls 2," and next if ( $category eq "Nohit");
        $gnuCmds .= " ls 3," and next if ( $category eq "Unassigned");
        if ( @colors ) {
            shift @colors;
            $gnuCmds .= " ls $lineNum";
            $lineNum++;
        }
        $gnuCmds .= ",";
    }
    $gnuCmds =~ s/,\s*$//;
    
    # Create image file using gnuplot.
    #
    my $gnuplotLogFile = "$outputDir/gnuplot.err.log";
    my $cmd = "| " . $GNUPLOT_EXE . " 2> $gnuplotLogFile";
    open CMD, $cmd;
    print CMD $gnuCmds;
    close CMD;

    # Remove log file if zero size.
    #
    unlink $gnuplotLogFile if -z $gnuplotLogFile;
    
    # Check if image file is created.
    #
    if ( checkFileExistence($imageFile) != SUCCESS ) {
        return FAILURE;
    }
    
    return SUCCESS;
}

#============================================================================#
sub getAvgGC {
    my $file = shift; # histogram file in text format
    
    my $avgGC = 0;
    open FH, $file or die "ERROR: failed to open file $file: $!\n";
    while (my $line = <FH>) {
        if ( $line =~ /Median: (\S+)/ ) {
            $avgGC = $1;
            last;
        }
    }
    close FH;
    
    return $avgGC;
}

#============================================================================#
sub getGcTotals {
    my $file = shift;
    
    my %gcDatas = ();
    open FH, $file or die "ERROR: failed to open file $file: $!\n";
    while (my $line = <FH>) {
        chomp $line;
        next if $line =~ /^#/;
        next if !length $line;
        my @vals = split /\s+/, $line;
        if ( @vals >= 7 ) {
            $gcDatas{$vals[1]} = $vals[6];
        }
    }
    close FH;
    
    return %gcDatas;
}

#============================================================================#
sub getSubsampleSize {
    my $file = shift;
    
    my $subsampleSize = 0;
    
    open FH, $file or die "ERROR: failed to open file $file: $!\n";
    while (my $line = <FH>) {
        chomp $line;
        if ( $line =~ /Number of datapoints: (\S+)/ ) {
            $subsampleSize = $1;
            last;
        }
    }
    close FH;
    
    return $subsampleSize;
}

#============================================================================#
sub sortCategoriesByArea {
    my @files = @_;
    
    my %fileAreas = ();
    
    foreach my $file (@files) {
        open IFILE, $file or die "Failed to open file $file: $!\n";
        my $area = 0;
        while (my $line = <IFILE>) {
            chomp $line;
            next if $line =~ /^#/;
            my @vals = split /\s+/, $line;
            next if @vals < 7;
            $area += $vals[1] * $vals[6];
        }
        $fileAreas{$file} = $area;
        close IFILE;
    }
    
    # Sort file by area size in ascending order, returning file name
    #
    my @sortedFiles = map {$_->[0] }
        sort {$a->[1] <=> $b->[1] || $a->[0] cmp $b->[0]}
        map { [$_, $fileAreas{$_}] } keys %fileAreas;
    
    return @sortedFiles;
}

#============================================================================#
sub createGcDataFile {
    my $file = shift;
    my $gcFile = shift;
    my $histFile = shift;
    my $binSize = shift;
    
    my $cmd = "cat $file | " . $GC_EXE . " -v | ";
       $cmd .= "grep -v 'G+C' > $gcFile;";
    print "$cmd\n\n";
    
    if ( system ( $cmd ) != 0 ) {
        print STDERR "$cmd failed to execute: $?\n";
        return FAILURE;
    }
    
    if ( -s $gcFile ) {
        my $cmd = $DRAW_HISTOGRAM_EXE . " -c 5 -b $binSize $gcFile > $histFile";
        print "$cmd\n\n";
        if ( system ( $cmd ) != 0 ) {
            print STDERR "$cmd failed to execute: $?\n";
            return FAILURE;
        }
    }
    
    if ( !-s $histFile ) {
        return FAILURE;
    }
    
    return SUCCESS;
}

#============================================================================#
sub createGcPlotFile {
    my $outputFile = shift;
    my $arefHistFiles = shift;
    my $hrefCategories = shift;
    my $hrefGcDatas = shift;
    my $subsampleSize = shift;
    
    if ( !open PLOTFH, ">$outputFile" ) {
        print STDERR "ERROR: failed to create plot file $outputFile: $!\n";
        return FAILURE;
    }
    
    my $title = '';
    foreach my $file (@$arefHistFiles) {
        my $category = $$hrefCategories{$file};
        $title .= "$category\t";
    }
    $title =~ s/\t$/\n/;
    $title = "# GC\t$title";
    print PLOTFH $title;
    
    foreach my $gc ( sort {$a<=>$b} keys %{$$hrefGcDatas{"Total"}} ) {
        my $line = "$gc\t";
        foreach my $file (@$arefHistFiles) {
            my $category = $$hrefCategories{$file};
            if ( $$hrefGcDatas{$category}{$gc} ) {
                $line .= sprintf( "%.3f\t", $$hrefGcDatas{$category}{$gc} /
                    $subsampleSize * 100);
            } else {
                $line .= "0\t";
            }
        }
        $line =~ s/\t$/\n/;
        print PLOTFH $line;
    }
    close PLOTFH;
    
    if ( checkFileExistence($outputFile) != SUCCESS ) {
        return FAILURE;
    }
    
    return SUCCESS;
}

#============================================================================#
sub checkFileExistence {
    my @files = @_;
    
    my $msg = "";
    foreach my $file (@files) {
        chomp $file;
        if ( !-s $file ) {
            print STDERR "Failed to create file $file\n.";
            return FAILURE;
        } else {
            $msg .= "Created $file.\n";
        }
    }
    
    print "$msg";
    
    return SUCCESS;
}

#============================================================================#
sub getCurrentDateAndTime {

# Returns current date and time in format:
# 'MM-DD-YYYY HH24:MI:SS'

    my ($sec,$min,$hour,$day,$mon,$year) = localtime(time);

    $mon++;
    $year+=1900;

    my $time = sprintf( "%02d/%02d/%04d %02d:%02d:%02d", 
        $mon,$day,$year,$hour,$min,$sec );

    return $time;

}
    
#============================================================================#
sub printhelp {
    my $verbose = shift || 1;
    pod2usage(-verbose=>$verbose);
    exit 1;
}

#============================================================================#
