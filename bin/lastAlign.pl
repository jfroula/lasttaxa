#!/usr/bin/env perl
=head1 NAME

lastAlign.pl

=head1 VERSION

1.2.1

=head1 SYNOPSIS

lastAlign.pl [options] <fasta>

  Options:
  -d    Last database path and prefix (required) (i.e. <path_to_nr_db_files>/nr (nr is the prefix here for all db files))
  -o    Directory to save results (*.results) and tmp files (required)
  -a    To re-use old files (optional)
  -s    prodigal protein predictions are subsampled by this number [default=no limit] (optional)
  -t    number of threads for lastal [1] (optional)
  -h    Detailed help message (optional).

  The fasta file used as the "last" database has to have NCBI formatted headers.  For instance,
  there must be >gi|<GI Number>|, otherwise a taxonomy will not be found.

  Example: 
  lastAlign.pl -d <directory_of_nr_db_files>/nr -o LastTaxa BON.1.fa

=head1 DESCRIPTION

This script will try to taxinomically classify a fasta file, where the
fasta file represents a full or parital genome created by a metagenomic 
binner like MetaBAT.  

I use "last" to find the homology between the fasta file
and NCBI's NR protein database.  Only if the query sequences are adequately covered 
by mapped proteins (i.e. proteins cover > 50% of the total contig bases)
will there be an attempt to classify the sequence.  Each protein's GI
number will be translated to a full taxonomy and the most frequent taxonomy at
each rank will be printed out, along with the percentage representing the top
taxonomic frequency.

To create the last database we ran this command on the UGE cluster (ram.c=40G,h_rt=12:00:00):
lastdb -p -c -m110 nr nr 

=head1 OUTPUT


=head1 ASSUMPTIONS

The database fasta file has to have NCBI like headers.  For instance
there must be a protien accession number immediately after the '>' and then a space, otherwise a taxonomy will not be found.
For example ">XP_642837.1 hypothetical protein DDB_G0276911 [Dictyostelium discoideum AX4]"

=head1 EXAMPLE

  lastAlign.pl \
  -d DEFAULT_NR/nr \
  -o Results \
  -t 16 \
  bin.10.fa

=head1 AUTHOR

=over 4

=item *

Jeff Froula - 08/18/2015 Creation

=back

=head1 COPYRIGHT AND LICENSE

Copyright (C) by DOE JGI

=cut

use strict;
use warnings;
use Pod::Usage;
use Getopt::Long;
use vars qw($optHelp $lastdb $outdir $use_old_files $subsample_num $threads);
use File::Basename;
use Carp;
use Cwd 'abs_path';
use Bio::SeqIO;


#============================================================================#
# INPUT VALIDATION
#============================================================================#
if( !GetOptions(
        "d=s"=>\$lastdb,
        "o=s"=>\$outdir,
        "s:i"=>\$subsample_num,
        "t:i"=>\$threads,
        "a"=>\$use_old_files,
        "h"=>\$optHelp
    )
) {
    pod2usage();
    exit 1;
}

our $fasta = shift @ARGV;

pod2usage(-verbose=>2) and exit 1 if defined $optHelp;
pod2usage(-verbose=>1) and exit 1 unless $lastdb && $outdir && $fasta;

our ($basename,$dirofile,$suffix) = fileparse($fasta, qr/\.[^.]*/);

#============================================================================#
# MAIN
#============================================================================

my $cmd;
my %taxonomy = ();
my %rank_hash = ();
my $minEval = 1e-3;
my $prots = 'prots.faa';
my ($lastdb_bname,$lastdb_path) = fileparse($lastdb);
my $max_bp_prodigal=30000000;


$fasta = abs_path($fasta);
$lastdb_path = abs_path($lastdb_path);
$lastdb = $lastdb_path . "/" . $lastdb_bname;
$threads=1 unless $threads;  

die ("File not found or empty: $fasta") unless -s $fasta;
die ("Directory not found: $lastdb_path/") unless -d $lastdb_path;
my @nrFiles = glob("$lastdb_path/$lastdb_bname.*");
die ("lastdb database files not found: i.e. $nrFiles[0]") unless -s "$nrFiles[0]";

#
# create the output dir
#
if (! -d $outdir){
	print STDERR "Failed to find $outdir....creating\n";
	mkdir $outdir || croak "failed to create $outdir\n";
}
chdir $outdir;

# handle compressed fasta files
if ($suffix eq '.gz'){
	$cmd="gunzip -c $fasta > $basename";
	print "## $cmd\n";
	croak "Failed $cmd\n" if (system($cmd));
	croak "Failed to find uncompressed fasta: $basename\n" unless -e $basename;
	$fasta=$basename;
	($basename,$dirofile,$suffix) = fileparse($fasta, qr/\.[^.]*/);
}elsif ($suffix eq '.bz2'){
	$cmd="bunzip2 -c $fasta > $basename";
	print "## $cmd\n";
	croak "Failed $cmd\n" if (system($cmd));
	croak "Failed to find uncompressed fasta: $basename\n" unless -e $basename;
	$fasta=$basename;
	($basename,$dirofile,$suffix) = fileparse($fasta, qr/\.[^.]*/);
}

#
# Make sure no sequence exceeds 32Mbases which makes prodigal complain
#
my $bname_fasta='';
if ( ! -s "$bname_fasta.trim.fasta" || ! $use_old_files){
	($bname_fasta) = basename($fasta);
	$cmd="reformat.sh -Xmx4g overwrite=t in=$fasta forcetrimright=$max_bp_prodigal out=$bname_fasta.trim.fasta";
	print "## $cmd\n";
	croak "Failed $cmd\n" if (system($cmd));
	verifyExistence("$bname_fasta.trim.fasta");
}else{
	print "## re-using fasta trimmed file: $bname_fasta.trim.fasta\n";
}

#
# predict proteins on fasta
#
if ( ! -s $prots || ! $use_old_files){
	# using translation table 1 for generic, where 11 is bacteria
	$cmd="prodigal -a $prots -g 1 -i $bname_fasta.trim.fasta -o prodigal.out";
	print "## $cmd\n";
	croak "Failed $cmd\n" if (system($cmd));
	verifyExistence($prots);
}else{
	print "## re-using prodigal predicted prots: $prots\n";
}

#
# run last 
#
if ( ! -s "$basename.maf" || ! $use_old_files){
	$cmd="lastal -P $threads $lastdb $prots > $basename.maf";
	print "## $cmd\n";
	croak "Failed $cmd\n" if (system($cmd));
	verifyExistence("$basename.maf");
}else{
	print "## re-using $basename.maf\n";
}


#============================================================================#
# SUBROUTINES
#============================================================================#

sub verifyExistence
{
	my ($file) = @_;
	croak "File missing or empty: $file\n"
	  if ( ! -s $file);
}

