#!/usr/bin/env perl
use strict;
use warnings;
my $taxon_table = $ARGV[0];
my %taxon_hash = ();
my $max=5;
die "\nUsage: $0 <taxon table from taxGcPlot.pl>\n\n" unless $taxon_table;
open FH,"$taxon_table" || die "$!\n";
while (<FH>){
	chomp;
	my @l=split(/\s+/);
	$l[3] = "na" unless ($l[3]);
	$taxon_hash{$l[0]}{$l[1]}=[$l[2],$l[3]];
}

my @ranks = qw(Kingdom Phylum Class Order Family Genus Species);
foreach my $rank (@ranks){
	my $i=0;
	my @sorted_tax = sort{ $taxon_hash{$rank}{$b}[0] <=> $taxon_hash{$rank}{$a}[0]} keys %{$taxon_hash{$rank}};
	my $total;
	map{ $total += $taxon_hash{$rank}{$_}[0] } keys %{$taxon_hash{$rank}};
	foreach my $taxon (@sorted_tax){
		next if $i >= $max;
		$i++;
		my $count = $taxon_hash{$rank}{$taxon}[0];
		my $per = sprintf("%0.2f",$count/$total*100);
		printf ("%s %-30s %d %-10d %-10.2f %s\n",$rank,$taxon,$count,$total,$per,$taxon_hash{$rank}{$taxon}[1]);
	}
	print "## -------------------------------------------------------------------------------------------- ##\n";
}
