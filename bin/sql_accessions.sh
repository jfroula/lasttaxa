CREATE TABLE accessions (accession text primary key, taxid integer);
.import acc2taxid accessions
CREATE UNIQUE INDEX index_acc on accessions (accession);
