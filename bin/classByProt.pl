#!/usr/bin/env perl
use strict;
use warnings;
my %taxon = ();

open FH,"$ARGV[0]" || die "$!\n";
while(<FH>){
	chomp;
	my ($gi,$tax,$fulltax) = split(/\t/);
	$taxon{$tax}{count}++;
	$taxon{$tax}{full} = $fulltax;
}

my @sorted_counts = sort{$taxon{$b}{count} <=> $taxon{$a}{count}} keys %taxon;

foreach my $tax (@sorted_counts){
	print "$taxon{$tax}{count}\t$taxon{$tax}{full}\n";
}

