#!/bin/bash
# Sometimes a job will crash. 
# Given a list of paths to each bin directory (i.e. Output_bin.1.fa), 
# this script checks that a finalSummary file was created; and if not, 
# the bin name will be printed out.

set -e

LIST=$1
if [[ ! $LIST ]]; then
    echo "Usage: $0 <list of fullpaths to bins>"
    exit 1
fi
if [[ ! -s $LIST ]]; then
    echo "$LIST is missing or empty"
    exit 1
fi

readarray FILES < $LIST

for f in "${FILES[@]}"; do
  BASENAME="${f##*/}"

  # Output_BON.221.fa/
  DIR="Output_${BASENAME}"
  DIR=$(echo $DIR | tr -d '\n')

  if [ -e $DIR ]; then
    if [ ! -f "$DIR/finalSummary" ]; then
        echo "No finalSummary for $DIR"
    fi
  else
    echo "Directory doesn't exist: $DIR"
  fi
done

