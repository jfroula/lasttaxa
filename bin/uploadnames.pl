#!/usr/bin/env perl

use strict;
use warnings;
use FindBin qw($RealBin);
use DBI;

my $names_file = "names.dmp";  

unless(-f $names_file) {
    print "cannot find $names_file\n";
    exit;
}
  
my $db = DBI->connect("dbi:SQLite:names.db", "", "", {RaiseError => 1, AutoCommit => 0});
$db -> do("drop table if exists taxtonames");
$db -> do("create table taxtonames(taxid integer primary_key, name text, name_lc text, type text)");

open(my $names_fh, $names_file) or die $!;
my $i = 0;
while(my $line = <$names_fh>){
    next if $line =~ /type material/;
	$line =~ s/[\[\]\"]//g;
    $line =~ s/\t\|\n//;
    $i++;
    my @fields = split(/\t\|\t/, $line);
    my $tax_id = $fields[0];
    my $name = $fields[1];
    my $name_lc = lc($fields[1]);
	my $type = $fields[3];
#    print "$tax_id NAME: $name LCNAME: $name_lc Field type: $type\n";
    $db->do("INSERT INTO taxtonames values(\"$tax_id\", \"$name\", \"$name_lc\", \"$type\")");   
    print "$i uploaded\n" if $i % 100000 == 1;
}

$db ->do("create index taxid_index on taxtonames(taxid)");
$db ->do("create index namelc_index on taxtonames(name_lc)");
$db->commit() or die $db->errstr;
