#!/usr/bin/env perl
#
# Summary:
# --------
# creates a table "finalSummary" that has a best guess of the 
# classification.
#
# This script will get a percentage of the top taxonomy vs. the total.  
# It will return a percentage for each taxa level.
# It will ignore any non-calls for the taxa names like 'NA'. If a species is like
# Bacteroidetes bacterium 35_15|NA|NA|NA|NA|Bacteroidetes|Bacteria 
# then there will only be data reported for bacterium 35_15, Bacteroidetes, 
# and Bacteria [species,phyla and superkingdom].
#
# Assumptions
# -----------
# that you ran classify_array.sh already.
#
# There needs to be a file called prots.faa.
# This is used to calculate total number of proteins
# predicted by prodigal.

use strict;
use warnings;
use File::Basename;

#============================================================================#
# INPUT VALIDATION
#============================================================================#
my $accNums = "accNums.tax";
die "Usage: $0\nExpects accNums.tax to exist.\nCreates a table called taxonomic.percents\n" 
  unless -s $accNums;

die "classificationByProtein missing\n" unless ( -e "classificationByProtein" ); 
die "prots.faa missing\n" unless ( -e "prots.faa" ); 

#============================================================================#
# MAIN
#============================================================================

our $tools=dirname($0);
my @ranks=qw(species genus family order class phyla superkingdom);
my @allTaxa;
my %counts = ();
my ($numElements,$last) = (0,0);
open ACC,"$accNums" || die "can't open $accNums\n";
while (<ACC>) {
    chomp;
    my @line = split(/\t/);
    my @tax = split(/\|/,$line[2]);

    $numElements = scalar @tax;
    die "There should be a consistent number of taxonomy fields for each annotation" .
        "$last != $numElements\n" unless $last == $numElements || $last == 0;

    for (my $i=0;$i<scalar @tax;$i++){
        $counts{$i}{$tax[$i]}++;
    }
    $last=$numElements;
}    
close ACC;
#use Data::Dumper;
#die Dumper(%counts),"\n";

# get total number of proteins predicted by prodigal.
# We should be able to count proteins from the prots.faa file
my ($prodigal_total);
$prodigal_total = `grep -c "^>" prots.faa`; 
chomp $prodigal_total;

# get the top taxa and some protein counts for the report
my ($topTaxa,$topTaxaProts,$sumHitProts) = calculateProteinNumbers();
my @final;

my @splitTopTaxa = split(/\|/,$topTaxa);

print <<EOF;
### Best guess of what query is:
$topTaxa

proteins predicted by prodigal: $prodigal_total
proteins that hit database: $sumHitProts
proteins that hit top taxa: $topTaxaProts

### breakdown by rank ###
# rank  name    top_hits    percent_from_dbhits  percent_from_all
# -----------------------------------------------------------------
EOF

for my $index ( sort {$a<=>$b} keys %counts){
    # we only want to get counts for taxa that was part of the full 
    # taxon string for the top hit.
    my $toptaxa = $splitTopTaxa[$index];


    # Lets check that I only have one taxa name as the hash key, so
    # I need to remove spaces for the hash keys and for the toptaxa so my grep
    # will work as expected. The spaces confuse grep for some multi word taxa names.
    my @nospaces_keys = map{ my $s=$_; $s =~ s/\s/_/g; $s } keys %{$counts{$index}};
    my $nospaces_top = $toptaxa;
    $nospaces_top =~ s/\s/_/g;

    my @a = grep{ m{\b$nospaces_top\b} } @nospaces_keys;

    die "too many grep hits: ",scalar @a,"\nquery: $toptaxa\nresults: @a\n" unless scalar @a == 1;

    # ignore where taxa is "NA"
    if (@a){
        if ($a[0] eq 'NA'){
            next;
        }
    }else{
        die "The taxa $toptaxa was not found in \%counts hash\n";
    }

    my $topcount = $counts{$index}{$toptaxa};
    
    # get percentage where denominator is all predicted proteins
    my $pertot = sprintf("%0.2f",$topcount/$prodigal_total*100);
    $pertot = "$pertot%";

    # get percentage where denominator is only proteins that hit database
    my $perhit = sprintf("%0.2f",$topcount/$sumHitProts*100);
    $perhit = "$perhit%";
    printf ("%-15s %-25s %10d %10s %10s\n",$ranks[$index],$toptaxa,$topcount,$perhit,$pertot);
}

print <<EOF;

Use "breakdown by rank" to get some idea how close your unknown genome is 
  to the reference at each taxonomic level. For example, if phyla is a high 
  percent in the percent_from_dbhits column like 88-99% but then drops to 60% at the class level then you may 
  have a novel class. 

Use the range between "percent_from_dbhits" and "percent_from_all" 
  for a taxa to represent what you can be confident of; since we don't 
  know if a protein didn't hit the database because the prodigal prediction was 
  wrong or because the protein wasn't represented in the database, we can calculate
  two numbers representing the upper and lower bounds.
EOF
#============================================================================#
# SUBROUTINES
#============================================================================
sub returnTopTaxa
{
    my ($sortedTaxa) = @_;
    my $top = shift @$sortedTaxa;
    if ($top eq 'NA'){
        $top = returnTopTaxa($sortedTaxa);
    }else{
        return $top;
    }
}

sub calculateProteinNumbers
{
    # number of protiens that hit top taxonomy string
    my $sum;
    my $topn=`head -1 classificationByProtein | awk '{print \$1}'`;
    chomp $topn;

    # top taxonomy string
    my $topt=`head -1 classificationByProtein | awk -F"\t" '{print \$2}'`;
    chomp $topt;
    $topt =~ s/,//;

    # total number of proteins that hit anything in database
    my $sum_line=`$tools/sum.pl classificationByProtein 1`; ###| perl -ne '/totalling\s+(\d+)/;print "$1\n";'`;
    if ($sum_line =~ /totalling\s+(\d+)/){
        $sum = $1;
    }else{
        die "failed to find the sum of proteins from first column of classificationByProtein\n";
    }

    return($topt,$topn,$sum);

}
