#!/usr/bin/env perl

use File::Basename;
use Getopt::Long;
use vars qw($optHelp,$optFull);

if( !GetOptions(
                "full"=>\$optFull,
                "h"=>\$optHelp,
               )
    ) {
    printhelp();
}

if ($optHelp || @ARGV < 1) {
    printhelp();
}

my ($maf,$minPctId) = @ARGV;

$minPctId = 0 if !defined $minPctId;

my $expect = 'N/A';
my %entries = ();
open IN, $maf or die "Failed to open file $maf:$!\n";
print "#query\tsubject\texpect\tlength\t%id\tq_length\ts_length\tscore\n";
while (my $line = <IN>) {
    chomp $line;
    if ($line =~ /^a score=(\d+)/) {
        my $tophitScore=$1;
        chomp($line = <IN>);
        my (undef,$subject,$sstart,$salnSize,$sstrand,$slen) = split /\s+/, $line;
        my $gi = (split/\|/,$subject)[1];
        chomp($line = <IN>);
        my (undef,$query,$qstart,$qalnSize,$qstrand,$qlen) = split /\s+/, $line;
        my $pctId = $qalnSize/$qlen*100;
        if ($pctId >= $minPctId) {
            $pctId = sprintf("%.2f",$pctId);
            if ($optFull) {
                print "$query\t$subject\t$expect\t$qalnSize\t$pctId\t$qlen\t$slen\t$tophitScore\n";
            } else {
                push @{$entries{$query}}, [$tophitScore,$subject,$expect,$qalnSize,$pctId,$qlen,$slen];
            }
        }
    }
}
close IN;

if (!$optFull) {
    foreach my $query (keys %entries) {
        my $best = (sort { $$b[0] <=> $$a[0] } @{$entries{$query}})[0];
        print $query;
        print map "\t$_", @$best[1..$#$best],"$best->[0]\n";
    }
}


sub printhelp {
    my $cmd = basename($0);
    print <<EOM;
Usage: $cmd [options] <last.maf> [<minPctId; default=0>]

Options:
    -all    Print all hits, not just best hit.
    -h      Help message.
EOM
    
    exit 0;
}

