#!/usr/bin/env perl
use warnings;


$n_args = @ARGV;
if (($n_args != 1) && ($n_args != 2)) {
    die "Usage: ./sum.pl <file> <<col>>\n";
}

open (F,$ARGV[0]) || die "Couldn't open $ARGV[0]\n";

my $col = 0;
if ($n_args == 2) {
    $col = $ARGV[1]-1;
}

my $sum = 0;
my $sum_sq = 0;
my $n = 0;
my ($mean, $std_dev);


while (my $i = <F>) {
    chomp $i;
    my @cols = split(/\s+/,$i);
    $sum += $cols[$col];
    $sum_sq += ($cols[$col])*($cols[$col]);
    $n++;
}

if ($n > 0)
{
    $mean = $sum/$n;
    $std_dev = sqrt($sum_sq/$n - ($sum/$n)*($sum/$n));
    printf("Found $n values totalling %.4f. <%.4f +/- %.4f>\n",
	   $sum, $mean, $std_dev);
}
else
{
    $mean = "NaN";
    $std_dev = "NaN";
    printf("Found $n values totalling %.4f. <%s +/- %s>\n",
       $sum, $mean, $std_dev);
}
 

close F;

    
