#!/bin/bash

usage()
{
cat<<EOF
  Usage: $0 [options] <-o outdir> <genome fasta>

   Options:
    <-o output dir (required)>
    <-d prefix of "last" formatted db of genomes. (i.e. some/path/nr)>
    <-t ncbi taxonomy dump directory (names.dmp & nodes.dmp)>
    <-q sqlite3 database of accession numbers to taxonIDs>
    <-c number of threads for "last" alignment [1]>

    defaults are shown in square brackets
	
  Example:  

    lastTaxa.sh \\
     -d nr_directory/nr \\
     -t ncbi_tax_dump_directory \\
     -q directory_to_accessions_db/accessions.db \\
     -o Testdir \\
     -c 16 \\
     test.fasta

EOF
exit 1
}

list=
lastdb=
taxonomydir=
threads=
bindir=

while getopts 'o:d:t:q:c:w' OPTION
do 
  case $OPTION in 
  d)    lastdb="$OPTARG"
        ;;
  o)    bindir="$OPTARG"
        ;;
  t)    taxonomydir="$OPTARG"
        ;;
  q)    accTaxProt="$OPTARG"
        ;;
  c)    threads="$OPTARG"
        ;;
  ?)    usage
        exit 1
        ;;
  esac
done

taxonomydir=$(realpath $taxonomydir)
nodesTax="$taxonomydir/nodes.dmp"
namesTax="$taxonomydir/names.dmp"

shift $((OPTIND - 1))
bin="$*"

# check for required arguments
if [[ ! $bin ]]; then
	echo "input fasta needs to be included (no flag needed)."
	usage
	exit 1
fi
if [[ ! $bindir ]]; then
	echo "Please set an output directory name with -o flag"
	exit 1
fi

# make sure we have dependent files
if [[ -d $bindir ]] || [[ $overwrite ]]; then
	echo "## $bindir already exists...overwritting it."
fi
mkdir -p $bindir

# create log
touch $bindir/run.log
LOG=$(realpath $bindir/run.log)
echo -e "#############################\nyou can check $bindir/run.log for errors\n#############################\n\n"


# check that we have all necessary scripts
# perl
path=$(which perl)
if [[ $? > 0 ]]; then
    echo "Please put perl in your PATH"
    exit 1
else
    echo "## found $path"
fi

# prodigal
path=$(which prodigal)
if [[ $? > 0 ]]; then
    echo "Please put prodigal in your PATH"
    exit 1
else
    echo "## found $path"
fi

# lastal
path=$(which lastal)
if [[ $? > 0 ]]; then
    echo "Please put lastal in your PATH"
    exit 1
else
    echo "## found $path"
fi

# bbtools
path=$(which reformat.sh)
if [[ $? > 0 ]]; then
    echo "Please put bbmap/reformat.sh in your PATH"
    exit 1
else
    echo "## found $path"
fi

## gnuplot
#path=$(which gnuplot)
#if [[ $? > 0 ]]; then
#    echo "Not required....but you need to put gnuplot in your PATH if you want the plots to be created."
#    exit 1
#else
#    echo "## found $path"
#fi

# lastTaxa.sh
path=$(which lastTaxa.sh)
if [[ $? > 0 ]]; then
    echo "Please put lastTaxa.sh in your PATH"
    exit 1
else
    echo "## found $path"
fi

if [[ ! -s $bin ]]; then
	echo "query input fasta file $bin doesn't exist (or is empty)." 
	exit 1
fi
if [[ ! -s "$lastdb.prj" ]]; then
	echo "failed to find lastdb file $lastdb.prj"
	exit 1
fi
if [[ ! -d "$taxonomydir" ]]; then
	echo "failed to find taxonomy directory: $taxonomydir"
	exit 1
fi
if [[ ! -e $accTaxProt ]]; then
	echo "failed to find required sqlite3 database: $database"
	exit 1
fi
if [[ ! -e $nodesTax ]]; then
	if [[ -e $nodesTax ]]; then
		nodesTax="$taxonomydir/nodes.dmp.gz"
	else
		echo "failed to find required file: $nodesTax (or the .gz version)"
		exit 1
	fi
fi
if [[ ! -e $namesTax ]]; then
	if [[ -e $namesTax ]]; then
		namesTax="$taxonomydir/names.dmp.gz"
	else
		echo "failed to find required file: $namesTax (or the .gz version)"
		exit 1
	fi
fi


bin=$(realpath $bin)
bname=$(basename $bin)
tools=$(realpath $0)
tools=$(dirname $tools)
threads=${threads:=1}
accTaxProt=$(realpath $accTaxProt)

start=$(date +%s)

echo -n "## running lastAlign.pl..."
echo    "## running lastAlign.pl" >> $LOG
if [[ $sub_sample_num != 'all' ]] ; then
    echo "$tools/lastAlign.pl -t $threads -d $lastdb -o $bindir -s $sub_sample_num $bin" >> $LOG
          $tools/lastAlign.pl -t $threads -d $lastdb -o $bindir -s $sub_sample_num $bin >> $LOG 2>&1
		  if [[ $? > 0 ]]; then echo Failed lastAlign.pl;exit 1; else echo done; fi
else
    echo "$tools/lastAlign.pl -t $threads -d $lastdb -o $bindir $bin" >> $LOG
          $tools/lastAlign.pl -t $threads -d $lastdb -o $bindir $bin >> $LOG 2>&1
		  if [[ $? > 0 ]]; then echo Failed lastAlign.pl;exit 1; else echo done; fi
fi

# this directory should have been made by lastAlign.pl
cd $bindir
echo "## changing into $bindir"

maf=$(echo *.maf)
if [ ! -s $maf ]; then echo "no maf file found";exit 1; fi

echo -n "## finding top last hits..."
echo    "## finding top last hits" >> $LOG
echo "$tools/last2tophits.pl $maf > topProteinHits" >> $LOG
      $tools/last2tophits.pl $maf > topProteinHits 2>> $LOG
if [[ $? > 0 ]]; then echo Failed finding top last hits;exit 1; else echo done; fi

#
# find full taxon from accession numbers 
#
echo -n "## parsing accession from fasta header..."
echo    "## parsing accession from fasta header..." >> $LOG
awk '{print $2}' topProteinHits | sed '1,1 d' > accNums.tab
if [[ $? > 0 ]]; then 
    echo "Failed to parse accession from header in topProteinHits."
    echo "Command was awk '{print \$2}' topProteinHits | sed '1,1 d' > accNums.tab"; 
else
 	echo "done"
fi

echo -n "## running acc2whole_taxonomy.pl..."
echo    "## running acc2whole_taxonomy.pl" >> $LOG
echo "$tools/acc2whole_taxonomy.pl --taxdump $taxonomydir --in accNums.tab --out accNums.tax --acc $accTaxProt" >> $LOG
      $tools/acc2whole_taxonomy.pl --taxdump $taxonomydir --in accNums.tab --out accNums.tax --acc $accTaxProt >> $LOG 2>&1
if [[ $? > 0 ]]; then echo Failed creating accNums.tab;exit 1; else echo done; fi

if [ ! -s "accNums.tax" ]; then echo "no accNums.tax file found";exit 1; fi

#
# get table of most frequent proteins
#
echo -n "## creating classificationByProtein..."
echo    "## creating classificationByProtein" >> $LOG
echo "$tools/classByProt.pl accNums.tax > classificationByProtein" >> $LOG
      $tools/classByProt.pl accNums.tax > classificationByProtein 2>> $LOG
if [[ $? > 0 ]]; then echo Failed creating classificationByProtein;exit 1; else echo done; fi

#
# This will create the finalSummary table
#
echo -n "## Running tools/taxonomyBreakdown.pl..."
echo    "## Running tools/taxonomyBreakdown.pl" >> $LOG
echo "$tools/taxonomyBreakdown.pl" >> $LOG
      $tools/taxonomyBreakdown.pl >finalSummary 2>> $LOG
if [[ $? > 0 ]]; then echo Failed running taxonomyBreakdown.pl;exit 1; else echo done; fi

##
## This will generate figures
##
#echo -n "## Running guessTaxon.pl..."
#echo    "## Running guessTaxon.pl" >> $LOG
#echo "$tools/guessTaxon.pl Data/stats.txt > classificationByRank" >> $LOG
#      $tools/guessTaxon.pl Data/stats.txt > classificationByRank  2>> $LOG
#if [[ $? > 0 ]]; then echo Failed creating classificationByRank;exit 1; else echo done; fi
#
#echo -n "## creating plots and stats.txt..."
#echo    "## creating plots and stats.txt" >> $LOG
#echo "$tools/taxGcPlot.pl -t 9 -ylabel \"Percent Gene Hits\" -title \"GC for submission.assembly.fasta\" -pctCutoff 0.5 -od Plots -dataDir Data -tax_dump $taxonomydir topProteinHits prots.faa contigs" >> $LOG
#      $tools/taxGcPlot.pl -t 9 -ylabel "Percent Gene Hits" -title "GC for submission.assembly.fasta" -pctCutoff 0.5 -od Plots -dataDir Data -tax_dump $taxonomydir topProteinHits prots.faa contigs >> $LOG 2>&1
#if [[ $? > 0 ]]; then echo Failed taxGCPlots.pl; fi

end=$(date +%s)
echo "time elapsed " $((end-start)) seconds >> $LOG
echo "time elapsed " $((end-start)) seconds
