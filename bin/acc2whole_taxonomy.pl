#!/usr/bin/env perl

=pod

=head1 NAME

acc2whole_taxonomy.pl

=head1 DESCRIPTION

	This script will take a list of NCBI PROTEIN accession numbers and find the taxonomic
	path from kingdom on down to the species level; 
	
	This script requires the TAXONOMIC dump files from NCBI
	("ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/taxdump.tar.gz")
	
	specifically (names*.dmp & nodes.dmp)

    This script also requires an accession database which has to be created.
    This database is used to find taxonomic IDs from protein accession numbers 
    that should be part of the fasta headers in the NR database.

    # download table and re-format it to save memory.   
    wget ftp://ftp.ncbi.nih.gov/pub/taxonomy/accession2taxid/prot.accession2taxid.gz
    zcat prot.accession2taxid.gz | awk -F"\t" '{print $2"|"$3}' > acc2taxid 

    # create sqlite3 table to get taxID numbers. 
    # The database name and table schema must be exactly as shown here.
    sqlite3 accessions.db
    CREATE TABLE accessions (accession text primary key, taxid integer);  
    .import acc2taxid accessions
    CREATE UNIQUE INDEX index_acc on accessions (accession);

    # in my example below, I've saved the accessions.db database in ~/ncbi/taxdump/

=head1 SYNOPSIS

    acc2whole_taxonomy.pl --taxdump <directory of NCBI taxdump files> --in <file of geneoids> --out <outfile of string> --acc <accession database>

=head1 EXAMPLES

    acc2whole_taxonomy.pl --taxdump ~/ncbi/taxdump/ --in accessions.txt --out tigrPhylum.txt --acc ~/ncbi/taxdump/accessions.db 

=cut

use strict;
use warnings;
use Pod::Usage;
use Getopt::Long;
use DBI;

# OPTIONS
my ( $accessionIDs, $outFile, $taxDump, $optHelp, $database);
GetOptions(
    'in=s'    	=> \$accessionIDs,
    'out=s'   	=> \$outFile,
    'taxdump=s' => \$taxDump,
    'acc=s'      => \$database,
    'help|h'  	=> \$optHelp
) or pod2usage(2);
pod2usage( -exitstatus => 0, -verbose => 2 ) and exit if $optHelp;
pod2usage( -exitstatus => 0, -verbose => 1 ) and exit unless $accessionIDs && $taxDump && $database;

die("Invalid options; try --help\n")
  unless $accessionIDs
  and $taxDump;

my @acceptable_ranks = qw(species genus family order class phylum superkingdom);
die ("This is not a valid directory: $taxDump") unless -d $taxDump;
die ("File missing or empty: $accessionIDs") unless -s $accessionIDs;
my $nodesFile = "$taxDump/nodes.dmp";
my $namesFile = "$taxDump/names.dmp";

#
# check existence of required files
#
if ( ! -e $nodesFile ){
	if( -e "$nodesFile.gz"){
		$nodesFile="$nodesFile.gz";
	}else{
		die "Failed to find $nodesFile or $nodesFile.gz\n";
	}
}

if ( ! -e $namesFile ){
	if( -e "$namesFile.gz"){
		$namesFile="$namesFile.gz";
	}else{
		die "Failed to find $namesFile or $namesFile.gz\n";
	}
}

if ( ! -e $database ){
	die "Failed to find $database\n";
}

my %names = ();
my %nodes = ();

# LOAD TAXONOMY
print "loading $nodesFile\n";
loadTax($nodesFile,\%nodes);
print "loading $namesFile\n";
loadNames($namesFile,\%names);


# LOAD GIS
my $in;
my %accessions = ();
if ($accessionIDs) { open( $in, '<', $accessionIDs ) or die($!) }
else         { $in = *STDIN }

print "loading $accessionIDs\n";
while (my $id = <$in>) {
	next if $id =~ /^$/;
    chomp $id;
	$id =~ s/\s//g;

    # extract accession from line whether its
    # old NCBI NR format or new.
    if ($id =~ /^gi\|\d+\|.*?\|(.*?)\|/){
        $id=$1;
    }
    
    $accessions{$id} = undef; 
}
close($in);

# LOOKUP TAX IDS
my $driver="SQLite";
my $dsn = "DBI:$driver:dbname=$database";
my $userid = "";
my $password = "";
my $dbh = DBI->connect($dsn, $userid, $password, { RaiseError => 1 }) or die $DBI::errstr;
print "Opened database: $database successfully\n";


foreach my $acc (keys %accessions){
	my $stmt = qq(SELECT taxid from accessions where accession = "$acc";);
	my $sth = $dbh->prepare( $stmt );
	my $rv = $sth->execute() or die $DBI::errstr;
	if($rv < 0){ print $DBI::errstr; }
	while(my @row = $sth->fetchrow_array()) {
		$accessions{$acc} = $row[0];
	}
}
$dbh->disconnect();

# LOOKUP LEVEL TAX IDS
my $out;
if ($outFile) { open( $out, '>', $outFile ) or die($!) }
else          { $out = *STDOUT }

print "finding taxonomy\n";
foreach my $acc ( keys %accessions ) {
    my $tax = $accessions{$acc};
    unless ($tax) {
        warn("TaxId undefined for $acc\n");
        next;
    }

	my %rank_hash = ();
    getTaxLevel( $tax,\%rank_hash,\%nodes,\%names);

    my @allTax;
    foreach my $rank (@acceptable_ranks){
        if (exists $rank_hash{$rank}){
            push(@allTax,$rank_hash{$rank});
        }else{
            push(@allTax,'NA');
        }
    }
    print $out $acc,"\t", $tax, "\t", join('|',@allTax),"\n";
}


##################
## SUBROUTINES  ##
##################

sub loadTax {
    my ($nodesFile,$nodes) = @_;
	if ($nodesFile =~ /.gz$/){
    	open IN, "zcat $nodesFile |" or die($!);
	}else{
    	open( IN, '<', $nodesFile ) or die($!);
	}
    while (<IN>) {
        chomp;
        my ( $taxId, $parentId, $rank ) = split(/\t\|\t/);
        $nodes->{$taxId} = [ $parentId, $rank ];
    }
    close IN;
}


sub loadNames {
    my ($namesFile,$names) = @_;
	if ($namesFile =~ /.gz$/){
    	open IN, "zcat $namesFile |" or die($!);
	}else{
    	open( IN, '<', $namesFile ) or die($!);
	}
    open( IN, '<', $namesFile ) or die($!);
    while (<IN>) {
        chomp;
        my ( $taxId, $taxName, $altName, $nameType ) = split(/\t\|\t/);
        $names->{$taxId} = [ $taxName, $nameType ] if $nameType =~ /scientific/;
    }
    close IN;
}

sub getTaxLevel {
    my ( $id,$rank_hash,$nodes,$names ) = @_;
    die unless $id;
    return unless $nodes->{$id};
    my ( $parentId, $rank ) = @{ $nodes->{$id} };

    while ( $parentId != 1) {
        return unless $nodes->{$id};
        ( $parentId, $rank ) = @{ $nodes->{$id} };
        $rank_hash->{$rank} = $names->{$id}[0] if exists $names->{$id};

        $id = $parentId;
    }
}


