#!/bin/bash -l
#SBATCH  -A gtrnd
#SBATCH --qos=genepool
#SBATCH --nodes=1
#SBATCH  -C haswell
#SBATCH --exclusive
#SBATCH --time=3:00:00
#SBATCH --job-name=lasttaxa
#SBATCH --license=SCRATCH
set -e

usage()
{
cat<<EOF
  ON CORI:
  -------------
  Usage: sbatch --array=1-X $0 [options] <file of paths to fastas>

   Options:
   ---------
    <-d prefix of "last" formatted db of genomes. (i.e. some/path/nr)>
    <-t ncbi taxonomy dump directory (names.dmp & nodes.dmp)>
    <-q sqlite3 database of accession numbers to taxonIDs>
    <-c number of threads for "last" alignment [1]>

    Where 'x' is number of fastas you need to process.
    Defaults are shown in square brackets.
	
  Example:  

    By default, each job will be submitted to exclusive.c with 32 processes.

    sbatch --array=1-2 lastTaxa_jobArray_cori.sh \\
     -d nr_last/nr \\
     -t ncbi_taxdump \\
     -q accessions.db \\
     -c 32 \\
     list_of_paths_to_fasta

EOF
exit 1
}

list=
lastdb=
taxonomydir=
threads=

if [[ ! "$@" ]]; then
    usage
fi

while getopts 'd:t:q:c:' OPTION
do 
  case $OPTION in 
  d)    lastdb="$OPTARG"
        ;;
  t)    taxonomydir="$OPTARG"
        ;;
  q)    accTaxProt="$OPTARG"
        ;;
  c)    threads="$OPTARG"
        ;;
  ?)    usage
        exit 1
        ;;
  esac
done


start=$(date +%s)

#
# the two databases hopefully should be updated once in a while and
# preferably at the same time
#
nodesTax="$taxonomydir/nodes.dmp"
namesTax="$taxonomydir/names.dmp"

shift $((OPTIND - 1))
list="$*"

# to run on genepool cluster

# make sure we have dependent files
if [[ ! $list ]]; then
	echo "list of bins not found"
	usage
	exit
fi
if [[ ! -s $list ]]; then
	echo "list file doesn't exist (or is empty)." 
	exit
fi
if [[ ! -s "$lastdb.prj" ]]; then
	echo "failed to find lastdb file $lastdb.prj"
	exit
fi
if [[ ! -d "$taxonomydir" ]]; then
	echo "failed to find taxonomy directory: $taxonomydir"
	exit
fi
if [[ ! -e $accTaxProt ]]; then
	echo "failed to find required sqlite3 database: $database"
	exit	
fi
if [[ ! -e $nodesTax ]]; then
	if [[ -e $nodesTax ]]; then
		nodesTax="$taxonomydir/nodes.dmp.gz"
	else
		echo "failed to find required file: $nodesTax (or the .gz version)"
		exit	
	fi
fi
if [[ ! -e $namesTax ]]; then
	if [[ -e $namesTax ]]; then
		namesTax="$taxonomydir/names.dmp.gz"
	else
		echo "failed to find required file: $namesTax (or the .gz version)"
		exit	
	fi
fi

bin=$(head -n $SLURM_ARRAY_TASK_ID $list | tail -1)
bin=$(realpath $bin)
bname=$(basename $bin)
bindir="Output_${bname}"
tools=$(realpath $0)
tools=$(dirname $tools)
threads=${threads:=1}

if [[ ! -s $bin ]]; then
	echo "query fasta missing or empty: $bin"
	exit	
fi

echo   "shifter --image=jfroula/lasttaxa:1.1.8 lastTaxa.sh -o $bindir -d $lastdb -t $taxonomydir -q $accTaxProt -c $threads $bin"
shifter --image=jfroula/lasttaxa:1.1.8 lastTaxa.sh -o $bindir -d $lastdb -t $taxonomydir -q $accTaxProt -c $threads $bin

if [[ $? == 0 ]]; then echo finished successfully; fi
