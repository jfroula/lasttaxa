#!/bin/bash
# This script changes the name of the finalSummary to something more specific, like BON_bin.1.fa_class.txt.
# It expects there to be an outputdir in the current directory that was produced by "lastTaxa_jobArray_cori.sh"
# e.g. Output_BON.219.fa

set -e

LIST=$1
if [[ ! $LIST ]]; then
    echo "Usage: $0 <list of fullpaths to bins>"
    exit 1
fi
if [[ ! -s $LIST ]]; then
    echo "$LIST is missing or empty"
    exit 1
fi

readarray FILES < $LIST

for f in "${FILES[@]}"; do
  BASENAME="${f##*/}"
  BASENAME=$(echo $BASENAME | tr -d '\n')

  # Look for a directory named like:
  # Output_BON.221.fa/
  DIR="Output_${BASENAME}"

  if [ -e $DIR ]; then
    if [ -f "$DIR/finalSummary" ]; then
        mv $DIR/finalSummary $DIR/${BASENAME}_class.txt
    else
        echo "No finalSummary for $DIR"
    fi
  else
    echo "Directory doesn't exist: $DIR"
  fi
done

